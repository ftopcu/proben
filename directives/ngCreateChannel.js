'use strict';

/* Directive */

var soManagerAppDirectives = angular.module('soManagerAppDirectives.addChnl', []);
// Directive which adds a "Add Channel" Button to the smart object form (step-1)
// This directive is only applicable to this form as it is performing DOM Manipulation and needs the necessary DOM layout given in step-1.html
soManagerAppDirectives.directive('ngCreateChannel', ['$compile',
	function($compile){
		return {
			restrict: 'EA',
			template: '<a class="btn btn-default" type="submit"><img src="/img/add_medium_white.svg" alt="Add Channel" class="button_image"/> <span>Add Channel </span></a>',
			link: function($scope, element, attrs){
				element.bind("click", function(){//Click Handler for Add Channel Button
					//Let's add a new channel Element to the custom Channel Array each Time !
					$scope.smartObject.customChannels[$scope.customChannelsCounter] = {"channelName": "", "channelDesc": "", "channelUnit": "", "channelType": ""};
					//Adding new DOM Elements after user is clicking to the button
					angular.element(document.getElementById("defChannels")).after($compile('<div class="form-group"><label for="soDesc" class="col-sm-2 control-label">Channel Name</label><div class="col-sm-8"><input type="text" class="form-control chstream" placeholder="Channel Name" ng-model="smartObject.customChannels['+$scope.customChannelsCounter+'].channelName"></div></div><div class="form-group"><label for="soDesc" class="col-sm-2 control-label">Channel Description</label><div class="col-sm-8"><input type="text" class="form-control" id="soStream" placeholder="Channel Description" ng-model="smartObject.customChannels['+$scope.customChannelsCounter+'].channelDesc"></div></div><div class="form-group"><label for="soDesc" class="col-sm-2 control-label">Channel Unit</label><div class="col-sm-8"><input type="text" class="form-control" id="soStream" placeholder="Channel Unit Name" ng-model="smartObject.customChannels['+$scope.customChannelsCounter+'].channelUnit"></div></div><div class="form-group"><label for="soDesc" class="col-sm-2 control-label">Channel Type</label><div class="col-sm-8"><input type="text" class="form-control" id="soStream" placeholder="Channel Type" ng-model="smartObject.customChannels['+$scope.customChannelsCounter+'].channelType"></div></div><hr class="form-group-divider">')($scope));					
					$scope.customChannelsCounter++;
				});
			}
		}
}]);


/* Directive */

// Directive which adds a "Add Action" Button to the smart object form (step-1)
// This directive is only applicable to this form as it is performing DOM Manipulation and needs the necessary DOM layout given in step-1.html
soManagerAppDirectives.directive('ngCreateAction', ['$compile',
	function($compile){
		return {
			restrict: 'EA',
			template: '<a class="btn btn-default" type="submit"><img src="/img/add_medium_white.svg" alt="Add Action" class="button_image"/> <span>Add Action </span></a>',
			link: function($scope, element, attrs){
				element.bind("click", function(){//Click Handler for Add Channel Button
					//Let's add a new channel Element to the custom Channel Array each Time !
					$scope.smartObject.customActions[$scope.customActionsCounter] = {"name": "", "description": "", "status": "idle",
					 "lastUpdate": Date.now(), "parameters": new Array(), "paramCounter": 0};
					//Adding new DOM Elements after user is clicking to the button
					angular.element(document.getElementById("defActions")).after($compile('<div class="action"><div class="form-group"><label for="soDesc" class="col-sm-2 control-label">Actuation Name</label><div class="col-sm-8"><input type="text" class="form-control chstream" placeholder="Actuation Name" ng-model="smartObject.customActions['+$scope.customActionsCounter+'].name"></div></div><div class="form-group"><label for="soDesc" class="col-sm-2 control-label">Actuation Description</label><div class="col-sm-8"><input type="text" class="form-control" id="soStream" placeholder="Description for Actuation" ng-model="smartObject.customActions['+$scope.customActionsCounter+'].description"></div></div><div class="form-group defParameters"><label for="soDesc" class="col-sm-2 control-label">Parameter(s)</label><div class="col-sm-8"><ng-action-parameters action="smartObject.customActions['+$scope.customActionsCounter+']" "></ng-action-parameters></div></div><hr class="form-group-divider"></div>')($scope));					
					$scope.customActionsCounter++;
				});
			}
		}
}]);


soManagerAppDirectives.directive('ngActionParameters', ['$compile',
	function($compile){
		return{
			restrict: 'EA',
			scope:{
				action: "="
			},
			transclude: true,
			template: '<a class="btn btn-default" type="submit"><img src="/img/add_medium_white.svg" alt="Add Parameters" class="button_image"/> <span>Add Parameters </span></a>',
			link: function($scope, element, attrs){
				console.log(element);
				element.bind("click", function(element){
					console.log(this);
					var index = $scope.actionIndex;
					var action = $scope.action;
					action.parameters[action.paramCounter] = {"name": "", "unit": ""};
					//Add basic parameters element to array
					//angular.element(this.parentNode.parentNode).after($compile(')($scope));
					angular.element(this.parentNode.parentNode).after($compile('<ng-parameter parameter="action.parameters['+action.paramCounter+']"></ng-parameter>')($scope));
					$scope.action.paramCounter++;
				});
			}
		}
}]);

soManagerAppDirectives.directive('ngParameter', ['$compile',
	function($compile){
		return{
			restrict: 'EA',
			scope:{
				parameter: "="
			},
			template: '<div> <div class="form-group" ><label for="soDesc" class="col-sm-2 control-label">Parameter Name</label><div class="col-sm-8"><input type="text" class="form-control chstream" placeholder="Parameter Name" ng-model="parameter.name"></div></div><div class="form-group"><label for="soDesc" class="col-sm-2 control-label">Parameters Unit</label><div class="col-sm-8"><input type="text" class="form-control chstream" placeholder="Parameter Unit" ng-model="parameter.unit" ></div></div></div>',
			/*controller: ['$scope', function($scope){

			}],*/
			link: function(scope, element,attrs){

			}
		}
}]);

