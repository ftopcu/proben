'use strict';

/* Controllers */

var soManagerAppDirectives = angular.module('soManagerAppDirectives.menu', []);

soManagerAppDirectives.directive('ngMenu', [
	function(){
		return {
			restrict: 'EA',
			scope: {
				logout: '&',
				redirectUrl: '@',
				redirectName: '@'
			},
			template: '<ul class="nav navbar-right"><div class="btn-group"><a href="" class="btn btn-default">Menu</a><a href="" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a><ul class="dropdown-menu"><li><a href="{{redirectUrl}}">{{redirectName}}</a></li><li class=""><a href="" ng-click="logout()">Logout</a></li></ul></div></ul>'
		}
}]);
