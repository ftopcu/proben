'use strict';

/* Controllers */

var soManagerAppDirectives = angular.module('soManagerAppDirectives.breadcrumb', []);

soManagerAppDirectives.directive('ngBreadcrumb', [
	function(){
		return {
			restrict: 'EA',
			scope: {
				activeMenu: '&',
				click: '@',
			},
			template: '	<ol class="breadcrumb"><li><a ng-click="click(activeMenu)">Composer</a></li><li><a ng-click="click(activeMenu)" ng-class="{active: activeMenu == "streams">Automations</a></li><li ng-class="{active: activeMenu == "streams"">Streams</li><li>Logout</li></ol>'
		}
}]);
