'use strict';

/* Directive */

var soManagerAppDirectives = angular.module('soManagerAppDirectives.cstmChart', []);
// Directive which adds a "Add Channel" Button to the smart object form (step-1)
// This directive is only applicable to this form as it is performing DOM Manipulation and needs the necessary DOM layout given in step-1.html
soManagerAppDirectives.directive('ngCustomChart', ['$compile',
	function($compile){
		return {
			restrict: 'EA',
			scope:{
				ngModel: '='
			},
			template: '<div google-chart chart="channelData" width="700" height="600" class="gchart"></div>',
			link: function($scope, element, attrs){
				$scope.channelname = attrs.channel;
				$scope.channelData = {};
				$scope.channelData.data = {};
				$scope.channelData.type = "LineChart";
				$scope.channelData.options = {
				  'title': '',
				  'legend': {
				    'position':'top',
				  },
				  'animation':{
				    'duration': 1000,
				    'easing': 'out',
				  },
				  'hAxis':{
				    'title': 'Date'
				  },
				  'vAxis':{
				    'title': $scope.channelname
				  },
				};
				$scope.$watchCollection('ngModel', function(newVal){
					if(newVal != undefined){
						var data = newVal;
						// var tmpArray = [];
						// for( var i=0;i<data.length;i++){
						// 	tmpArray.push({"date": data[i].lastUpdate, "value": data[i].channels[$scope.channelname]["current-value"]});
						// }
						$scope.channelData.data = {"cols":[
							{id: "t", label: "Date", type: "date"},
							{id: "s", label: "Latitude", type: "number"}
						], "rows":[]};
						for(var i=data.length-1;i>=0;i--){
							var date = data[i].lastUpdate;
                			if(date.toString().length === 10){ //It is possible that some unix timestamps are shorter, the last zero dots for milliseconds are clipped
								var row = {c:[
								  {v: new Date(data[i].lastUpdate*1000)}, 
								  {v: data[i].channels[$scope.channelname]["current-value"]},
								]};                				
                			}else{ 
								var row = {c:[
								  {v: new Date(data[i].lastUpdate)}, 
								  {v: data[i].channels[$scope.channelname]["current-value"]},
								]};
                			}
                			$scope.channelData.data.rows.push(row);
						}
						$compile(element.contents())($scope);
					}
				});
			}
		}
}]);