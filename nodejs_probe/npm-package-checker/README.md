## NPM Package checker

The current version (0.1.0) is only tested for the package bootstrap-sass, but it is generally possible to check for file changes for any npm package.

This script is independent from any other project, therefore you have do some configuration:

- Add the paths to the package.json, related local node_module files you want to check (not yet supporting directories).

An example for config entries:

    var config = {
	    "packageName": "bootstrap-sass",
	    "pathToPackJson": "/Users/ftopcu/Desktop/Projekte/git-intern/vw-ngw-cms/ngw/ngw-frontend/package.json",
	    "filesToCheckLocal": ["/Users/ftopcu/Desktop/Projekte/git-intern/vw-ngw-cms/ngw/ngw-frontend/node_modules/bootstrap-sass/assets/stylesheets/bootstrap/_variables.scss", "/Users/ftopcu/Desktop/Projekte/git-intern/vw-ngw-cms/ngw/ngw-frontend/node_modules/bootstrap-sass/assets/stylesheets/bootstrap/_mixins.scss"],
	    "filesToCheckRemote": ["https://raw.githubusercontent.com/twbs/bootstrap-sass/master/assets/stylesheets/bootstrap/_variables.scss", "https://raw.githubusercontent.com/twbs/bootstrap-sass/master/assets/stylesheets/bootstrap/_mixins.scss"],
	    "filesTempName": ["_variables.scss", "_mixins.scss"]
    };

In the current version it is important to place the files, the remote urls for the npm package in the same ordner within the array.

Any ideas, feature request to ftopcu@pro-vision.de