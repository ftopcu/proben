/**
 * @author: Ferit Topcu
 * @date: 05.03.2015
 * @version 0.0.1
 *
 * This node script checks for updated versions of the bootstrap-sass script
 */
// Required packages
var config = require('./config.js');
var pjson = require(config.pathToPackJson);
var semver = require('semver');
var exec = require('child_process').exec;
var https = require("https");
var diff = require('diff');
var fs = require("fs");
/*
* Variables
 */
//name of package (bootstrap-sass)
var packageName = config.packageName;
//read out used package version number from package.json
var pkgVersion = pjson.dependencies[packageName].slice(1);
//Path to local files to compare with remote files
var localFiles = config.filesToCheckLocal;
//UrL's to the files which have to be compared with our own version
var remoteFiles = config.filesToCheckRemote;
var body = {};

/**
 * showView Function handles the output from npm view from the terminal
 * @param  {[String]} err    [description]
 * @param  {[String]} stdout [Output of terminal for npm view]
 * @param  {[String]} stderr [Error message of terminal]
 */
var showView = function (err, stdout, stderr){
	if(err !== null){
		console.log("exec error: " + err);
	}
	//Retrieving the version Number from the stdout String 
	//ToDo: Better possibility ?
	//Get only version Number line
	var substring = stdout.substr(stdout.indexOf("version:"), 20);
	//Split String (version Number is at place 1)
	var versionNumber = substring.split(": ")[1];
	//Clear npm view \' caps
	var cleanVersionNumber  = versionNumber.split("\'")[1];

	//Check if versionNumber is correct transformed
	if(semver.valid(cleanVersionNumber) !== null){
		if(semver.lt(pkgVersion, cleanVersionNumber)){
			var message = "[WARN:] A new version of " + packageName + " is available. Latest version on npm: " + cleanVersionNumber + " whereas the current project is using version " + pkgVersion;
			console.warn(message);
			//Iterate over remote files
			remoteFiles.forEach(function(element, index){
				console.info("[INFO]: Retrieving file: " + element);
				//Check for possible file changes
				//First get file
				https.get(element, function(res){
					var fileStream = fs.createWriteStream(config.filesTempName[index]);
					//write to filestream
					res.pipe(fileStream);
					//Wait for finishing the write
					res.on('end', function(){
						fileStream.end();
						fileStream.on('finish',function(){
							console.log("[INFO:] Writing to temp file finished.");
							//Temporary file is written & we now where our own package _variables file is, lets prettydiff
							//Read local file & remote retrieved file
							var oldFile = fs.readFileSync(localFiles[index]).toString();
							var newFile = fs.readFileSync(config.filesTempName[index]).toString();
							console.log(oldFile.length , newFile.length);
							console.info("[INFO:] Starting diff check of file: ", config.filesTempName[index]);
							//Making a diff between lines
							//This method returns differences between lines which allows an easier overview of added / removed / changed lines in the files
							//ToDo: Allow change of diff algorithm in config file
							var diffs = diff.diffLines(newFile, oldFile);
							//Return console prompt if no file changes exist
							if(diffs.length <= 1){
								console.log("[INFO:] There are no differences between the local and latest origin file.");
							}else{
								//Show changes
								console.log("[WARN:] There are " + diffs.length+ " changes between the local and latest origin file.");
								console.log("[WARN:] /--------- Changes for file " + config.filesTempName[index] + " -------------/ \n ");
								diffs.forEach(function(part){
									if(part.added){
										console.log("[INFO:] Line added: ", part.value);
									}
									if(part.removed){
										console.log("[INFO:] Line removed: ", part.value);
									}
								});
							}
						});
					});
				})
				.on("error", function(err){
					console.error("Error:" , err);
				});
			});
		}else{
			var message = "[INFO:] Up-to-date . Latest version on npm: " +cleanVersionNumber+ " and your current version " + pkgVersion;
			console.log(message);
		}
	}
};

/**
 * Main Execution of script (call of npm view)
 */

console.log("[INFO:]Starting check for package " + packageName + " v: " + pkgVersion);
console.log("[INFO:] Calling npm view " + packageName);
var cmd = "npm view " + packageName;
var child = exec(cmd, showView);
