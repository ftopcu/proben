/**
 * Config file for checkPackage script
 * users are able to define the package-name
 * and a list of files which should be checked
 * @author: Ferit Topcu 
 * Add all files to compare in the same position in the different arrays, e.g. 
 * filesToCheckLocal: ["abc.css"], filesToCheckRemote: ["pro-vision.de/abc.css"], filesTempName: ["abc_temp.css"]
 */
var config = {
	"packageName": "bootstrap-sass",
	"pathToPackJson": "",
	"filesToCheckLocal": [],
	"filesToCheckRemote": [],
	"filesTempName": []
};

module.exports = config;