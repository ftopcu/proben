/**
 * Created by Ferit on 17.04.2015.
 */
var https = require('https');
var request = require('request-promise');
var _ = require('lodash');

//var Passport = require('../models/Passport');
var passes = {};

var AnalyticController = {

  index: function(req, res)
  {
    if(!req.user) res.redirect('/auth/instagram');
    else Passport.getToken(req.user.id,function(pass){
      passes[req.user.id] = pass;
      res.send(pass);
    });
  },

  media: function(req, res)
  {
    var pass = passes[req.user.id];
    // https://api.instagram.com/v1/users/'+pass[0]+'/follows?access_token='+pass[1]
    var path = 'https://api.instagram.com/v1/users/'+483590318+'/followed-by?access_token='+pass[1];

    (getAllFollowers(req.user.id, path,[],false, res, function(result){
      res.send("done");
    }));


  },

  getMedia: function(req, res)
  {
    Profile.getProfile(req.user.id,function(result){
      res.send(result);
    });
  },

  test: function(req,res){
    res.send(passes[req.user.id]);
  }
};

function getAllFollowers(id, url, data, done, res, cb)
{
  if(done) return data;
  else {
    //var path = 'https://api.instagram.com/v1/users/'+pass[0]+'/follows?access_token='+pass[1];

    request(url)
      .then(function(resp){
        var dat = JSON.parse(resp);
        data.push(dat.data);
        Profile.setProfile(id, _.flatten(data), function(result){
          console.log("updated!");
        });
        if(dat.pagination.next_url) getAllFollowers(id, dat.pagination.next_url, data, false, res, cb);
        else {
          Profile.setProfile(id, _.flatten(data), function(result){
            console.log("got all the data : "+id);
            cb(result);
          })
        };
      });
  }
}

module.exports = AnalyticController;


module.exports = {};
