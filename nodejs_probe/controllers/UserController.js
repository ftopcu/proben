/**
 * Created by Ferit on 17.04.2015.
 */
var rp = require('request-promise');
var parser = require('../parser/parser.js');
var tagParser = require('../parser/tagparser.js');
//var Profile = require('../models/Profile.js');

var UserController = {
  getCurrent: function(req, res){
    //if(!req.user){ // nicht angemeldet
    //  res.send('unauthorized', 401);
    //  return;
    //}
    User.findOne(req.session.passport.user, function(err, resl){
      res.send(resl);
    })
  },

  getInstaData: function(req, res) {

    Profile.findOne({user: req.session.passport.user}, function (err, result) {
      if (typeof result == 'undefined') res.send('not found', 404);
      else res.send(result.data.data); // user profile
    });
  },

  startRefresher: function(req, res){
    User.findOne(req.session.passport.user, function(err, user){
      parser.refreshInstaData(user, req.params.interval, function(err, result){

        if(err){
          console.error(err);
          res.send(err, 500);
        }
        else {
          res.send(result,200);
        }

      });
    })

  },

  addHashTracker: function(req, res){
    var hashtag;
    if(typeof req.params.hashtag == "undefined")  res.send("Please provide Hashtag", 402);
    else {
      hashtag = req.params.hashtag;

      tagParser.addTracker(hashtag, function(err, answer){
        if(err) res.send(err, 400);
        else res.send(answer, 200);
      })
    }
  },

  hashTrackerCallback: function(req, res){
    var data = {
      challenge: req.query["hub.challenge"],
      mode: req.query["hub.mode"],
      verify_token: req.query["hub.verify_token"]
    };
    tagParser.handleCallback(data, function(err, resl){
      if(err) res.send('', 500);
      else {
        res.send(resl);
      }
    })
  },

  handleNewTagInfo: function(req, res){
    console.log(req.body);
    res.send("");
  }
};

module.exports = UserController;

