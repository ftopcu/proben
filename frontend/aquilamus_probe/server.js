
/**
 * Module dependencies.
 */

var express = require('express');

var http = require('http');
var path = require('path');
var querystring = require('querystring');
var Q = require("q");
var util = require('util');
var redNodes = require('./node_modules/node-red/red/nodes');
var request = require('request');
var digestAuthRequest = require('./app/apis/digestAuthRequest.js');
var userAPI = require('./app/apis/userAPI.js');
var servioticyUA = require('./app/apis/servioticyUA.js');
var skynet = require('meshblu-client');
var mongoose = require('mongoose');
var config = require('./app/config/config.js');

// connect to mongodb
mongoose.connect('mongodb://' + config.mongo.hostname + ':' + config.mongo.port + '/' + config.mongo.db);

// mongoose models
var User = require('./app/models/user')

/*var passport = require('passport'), 
LocalStrategy = require('passport-local').Strategy;*/
var app = express();
var server = http.createServer(app);
//var io = require('socket.io').listen(server);

var createcso = require('./app/routes/createcso');
var converter = require('./app/routes/getallsos');

/*****************************************************************************
------------------------ Basic Configuration Express ------------------------
*****************************************************************************/

// all environments
app.set('port', config.port);
app.set('views', path.join(__dirname, '/composer/views'));
app.set('view engine', 'jade');
app.set('hostname', config.hostname);
app.set('meshblu.port', config.meshblu.port);
app.set('meshblu.hostname', config.meshblu.hostname);
app.set('compose', config.servioticy);
app.set('iserve', config.iserve);
app.set('login', config.login);
app.set('idm', config.idm);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser());

// required for passport
app.use(express.session({ secret: 'gluethingsisrockingyouall' })); // session secret
/*app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions*/

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// enalbe CORS
app.all('*', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "PUT,GET,POST,DELETE");
	res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Accept,Accept-Charset,Content-Length,Authorization");
	next();
});

/*****************************************************************************
------------------------ Manager ------------------------------------------
*****************************************************************************/


// skynet
var conn = skynet.createConnection({
  // "uuid": "",
  // "token": "",
  "server": config.meshblu.hostname, // optional - defaults to ws://skynet.im
  "port": config.meshblu.port  // optional - defaults to 80
});

// Index-Page | Homepage
app.get('/', function(req, res){
  res.sendfile(__dirname + '/public/index.html');
});

//Registration API for smart object manager
//For security reasons, the registration is currently realized server-side
app.post('/api/register/:user/:pwd', function(req, res){

	var email = req.params.user;
	var password = req.params.pwd;
	var userData = {
		email: email,
		password: password,
		servioticy: {},
		gluethings: {},
		idm: {}
	};
	//IDM Support
	userAPI.register( email, password , function( err , data){
		if( !err ){
			userData.idm.id = data._id;
			userData.idm.username = data.username;
			//registerUser (promise) on "success"
			servioticyUA.registerUser(email, password).then(function(apiToken){
				userData.servioticy.token = apiToken;
				// register meshblu user
				conn.register({'type': 'user', 'email': email }, function(data){
					userData.gluethings = {
						uuid: data.uuid,
						token: data.token
					};
					//Save to DB

					var user = new User(userData);
					user.save();

					userData.gluethings.uuid = data.uuid;
					userData.gluethings.token = data.token;

					res.status(201).send(userData);
				});
			});
		} else {
			res.send(500, err);
		}
	});
});

//Basic servioticy login (without pw !)
//Login API for smart object manager
//Basically only the Email adress is mandatory to retrieve an API Token
app.post('/api/login/:email/:pwd', function(req, res){
	var email = req.params.email;
	var password = req.params.pwd;
	var apiToken = "";
	if(req.session.apiToken !== undefined && req.session.user !== undefined && req.session.apiToken !== ""
	&& req.session.apiToken !== "Invalid mail" && req.session.apiToken !== "Someone already has that email"){
	 	//User is registered or has been logged in before
		res.status(201).send(req.session.user);
	}else{
		userAPI.login( email, password, function( err, authData ){
			if( !err ){
				var idm = authData;
				User.findOne({'email': email}, function(err, user){
					console.log(err);
					console.log(user);
					if(user == null){ //Something went wrong -> user is not registered
						//ToDo: Does the user exist in servioticy before ? Check that, if true create new user -> "register"
						res.status(500).send({"token": 0});
					}else if(typeof user.gluethings.uuid == "undefined"){ //Meshblu UA is missing
						// register meshblu user
						conn.register({'type': 'user', 'email': email }, function(data){
							user.gluethings.token = data.token;
							user.gluethings.uuid = data.uuid;
							user.save();
						});
					}else if(typeof user.idm.token == "undefined"){
						//Only IDM Access Token is missing -> first login after registration
						user.idm.token = idm.accessToken;
						user.idm.token_type = idm.token_type;
						user.save();
					}
					// login to servioticy
					servioticyUA.loginUser(email, password).then(function(apiToken){
						user.servioticy.token = apiToken;
						user.save();

						//We have all the necessary data so just create the session object and return the "token"
						//currently, we are returning servioticy token as apiToken
						req.session.apiToken = user.servioticy.token;
						req.session.user = {};
						req.session.user.gluethings = user.gluethings;
						req.session.idm = user.idm;
						res.status(201).send({"token": user.servioticy.token});
					});
				});
			} else {
				res.status(500).send({"token": 0});
			}
		});
	}
});

//Login API for smart object manager
//Basically only the Email adress is mandatory to retrieve an API Token
//ToDo: Delete User from all backends and our own mongoDB cache
app.post('/api/delete/:user/:pwd', function(req, res){
	userAPI.delete( req.params.user, req.params.pwd, function( err ){
		if( !err ){
			res.status(204);
		} else {
			res.status(500).send('Error: delete request failed.');
		}
	});
});


app.post('/api/logout', function(req, res){
	req.session.apiToken = undefined;
	req.session.user = undefined;
	req.session.idm = undefined;
	res.status(201).send({"token": 0, 'gluethings': {}, 'idm':{}});
});

app.get('/api/loggedin', function(req, res){
	if(req.session.apiToken !== undefined && req.session.user !== undefined && req.session.apiToken !== ""
	&& req.session.apiToken !== "Invalid mail" && req.session.apiToken !== "Someone already has that email"){
		res.status(201).send({"token": req.session.apiToken, "gluethings": req.session.user});
	}else{
		res.status(400).send({"token": 0, 'gluethings': {}, 'idm':{}});
	}
});

app.get('/subscriptionUpdate/:id/:stream', function(req, res){
	//We don't know what kind of structure we will get back, so let's support gyroscope & location
	var soID = req.params.id;
	var stream = req.params.stream;
	console.log("ID: " + soID + " , Streamname: " + stream);
	if(stream === "location"){
		var latitude = req.query.latitude;
		var longitude = req.query.longitude;
		console.log("GEO: " + latitude + " , " + longitude);
		// io.sockets.on('connection', function (socket) {
		// 	socket.broadcast.to(soID).emit('updateSO', {latitude: latitude, longitude: longitude});
		// });
		//io.sockets.in(soID).emit('updateSO', JSON.stringify({latitude: latitude, longitude: longitude}));
	}else if(stream === "orientation"){
		var x = req.query.x;
		var y = req.query.y;
		var z = req.query.z;
		console.log("Gyro: " + x + " , " + y + " , "+z);
		//io.sockets.in(soID).emit('updateSO', {x: x, y: y, z:z});		
	}
	//console.log(req);
});


/*****************************************************************************
------------------------ COMPOSER ------------------------------------------
*****************************************************************************/



app.get('/createcso/*', createcso.create);

var RED = require("node-red");

server.listen(app.get('port'), function(){
  util.log('[server] Express server listening on port ' + app.get('port'));
});

//setTimeout(function(){
// Create the settings object
	var settings = {
	/*	// path to find and load the nodes in the nodes directory
		httpRoot: "/red",*/
		// route to access the Composer UI
	    httpAdminRoot:"/composer",
	    // root url for nodes that provide HTTP endpoints. Default: ‘/’
	    httpNodeRoot: "/api",
	    // relative userDir where Node-Red saves the Flows ( "compositions" )
	    userDir: "./composer/public/flows/",
	    functionGlobalContext: { meshblu: require('meshblu-client')},
	    connection: null,
	    userCredentials: {}
	};
	
	// Initialise the runtime with a server and settings
	RED.init(server, settings);

	// Serve the editor UI from /red
	app.use(settings.httpAdminRoot, RED.httpAdmin);

	// Serve the http nodes UI from /api
	app.use(settings.httpNodeRoot,RED.httpNode);	

	RED.start();
	
	/*
	// convert all smart objects before Node-Red starts
	var initialConvertingSmartObjects = function(){
		var when = require("when");
		converter.convertAllSmartObjects().then(function( convertingCompleted ){

			// Start the Node-Red runtime
			RED.start().then(function(){
				redNodes.loadCustomNodes().then(function(nodeErrors) {
		            if (nodeErrors.length > 0) {
		                util.log("[nodebuilder] Error converting nodes :"+err);
		                res.send(500);
		            } else {
		                fs.readdir(nodePath, function(err, files) {
			                if(err) throw err;
			                var i = 1;
			                for (i = files.length - 1; i >= 0; i--) {
			                    util.log("[server] Register config for "+files[i].split('_')[0]+'.'+files[i].split('.')[1]);
			                    // ----
			                    if(files[i] == '.' || files[i] == '..' || files[i] == undefined){
			                         // same amount undefined files as counted files
			                    } else {
			                        fs.readFile( nodePath+files[i], function(err, file) {
			                            if(err) throw err;                                
			                            // ----
			                            var filename = files[i];
			                            if(filename == undefined){
			                                // same amount undefined files as counted files
			                            } else {
			                                var extension = files[i].split('.');
			                                if(extension[1] == 'html'){
				                                // register config
			                                    redNodes.registerNodeConfig( file );  
			                                }
			                            }
			                        });   
			                    }                     
			                }                       
			            });                    
			        }
			    },function( failedToLoadCustomNodes ){
			    	util.log("failed to load custom nodes: " + failedToLoadCustomNodes );
			    });
			});			
		}, function( convertingFailed ){
			util.log("[converter] Failed to convert smart onjects");
		});
	}(); */
//}, 10000);

//server.js
