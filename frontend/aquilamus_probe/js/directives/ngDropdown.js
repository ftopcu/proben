'use strict';

/* Directive */

var demoAppDirectives = angular.module('demoApp.dropdown', []);

demoAppDirectives.directive('ngDropdown', [
	function(){
		return {
			restrict: 'EA',
			scope:{
				ngModel: '=',
				ngData: '=',
			},
			template: '<select class="form-control" id="select" ng-model="ngData" ng-options="t.name for t in ngModel"><option value="">--- choose an option ---</option></select>'
		}
}]);


