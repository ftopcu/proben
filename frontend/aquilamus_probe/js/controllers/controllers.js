/**
 * This the main controller.
 * It triggers the different services and provides some basic UI functions 
 *
 * @see services
 */

(function(){

'use strict';

/* Controllers */

var demoAppControllers = angular.module('demoAppControllers', []);

/**
** Helper Function to remove an object from an Array
*/
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

/*Our main app controller
* Provides all necessary functions for the demo Application
*/
demoAppControllers.controller('mainController', ['$scope', '$rootScope','Artwork','Material', 'Medium',
  function($scope, $rootScope, Artwork, Material, Medium){
  	/** Definition of parameters*/
  	$scope.artworks = new Array();
  	$scope.cacheArtworks = {};
  	$scope.isNewArtwork = false;
  	var limitElements = 10;
  	var lastIndex = 0;
  	$scope.selectedMedium = { "element": ""};
  	$scope.mediumSelection = new Array();
  	$scope.dimensionUnits = [
  		{"name": "cm"}, 
  		{"name": "inches"}];
  	$scope.selectedUnit = { "element": ""};
  	//New Entry
  	$scope.artwork = {
  		"artist": "",
  		"title": "",
  		"year": "",
  		"price": "",
  		"description": "",
  		"includes_vat": "",
  		"vat": "",
  		"dimension1":"",
  		"dimension2":"",
  		"dimension3":"",
  		"dimensions_in_cm": false,
  	};

  	//Get list of all artworks
  	Artwork.getArtworks().then(function(artworks){
  		$scope.cacheArtworks = artworks;
  		//API returns only HTTP-Endpoints for Details -> Iterate over array and retrieve list
  		//To improve speed loading only a first bunch of data
  		var i = lastIndex;
  		//There could be less artworks in the DB than our limit
  		if(limitElements>= $scope.cacheArtworks.urls.length){
  			limitElements = $scope.cacheArtworks.urls.length;
  		}
  		for(;i < limitElements;i++){
  			var url = $scope.cacheArtworks.urls[i];
  			Artwork.getArtwork(url).then(function(elem){
  				var artwork = elem;
  				artwork.materialList = new Array();
  				//Retrieve Array with HTTP Endpoints for Materials
  				//After that request information for each Material
  				var promMaterial =Material.getMaterials(artwork.materials).then(function(materialsArray){
  					materialsArray.urls.forEach(function(url, index, array){
  						Material.getMaterial(url).then(function(material){
  							artwork.materialList.push(material);
  						});
  					});
  				});
  				//After all materials are returned
  				//Get Medium
  				promMaterial.then(function(){
  					Medium.getMedium(artwork.medium).then(function(medium){
  						artwork.mediumList = medium;
  							$scope.artworks.push(artwork);
  					});
  				});
  			});
  		}
  		lastIndex = i;
  	});
	
	/**
	** LoadMore loads additional entries
	** This prevents the page to load all entries / or to display all entries immediately
	**/
  	$scope.loadMore = function(){
  		//ToDo: Retrieve new artwork list -> it is possible that there was an update (from another user)
  		var i = lastIndex;
  		//We need a breaking statement
  		if(lastIndex+limitElements>= $scope.cacheArtworks.urls.length){
  			limitElements = $scope.cacheArtworks.urls.length
  		}
  		for(var i =lastIndex;i < limitElements;i++){
  			var url = $scope.cacheArtworks.urls[i];
  			Artwork.getArtwork(url).then(function(elem){
  				var artwork = elem;
  				//Get material
  				var promMaterial = Material.getMaterial(artwork.materials).then(function(materials){
  					artwork.materialList = materials;
  				});
  				promMaterial.then(function(materials){
  					Medium.getMedium(artwork.medium).then(function(medium){
  						artwork.mediumList = medium;
  						$scope.artworks.push(artwork);
  					});
  				});
  			});
  		}
  		lastIndex = i;
  	}

  	/**
	** Create new artwork
  	**/
  	$scope.showArtwork = function(){
  		if($scope.isNewArtwork){
  			$scope.isNewArtwork = false;
  		} else{
  			$scope.isNewArtwork = true;
  		}
  		$scope.mediumSelection = new Array();
  		//Get Medium list
  		Medium.getMediums().then(function(mediumUrlList){
  			mediumUrlList.urls.forEach(function(url, index, array){
  				Medium.getMedium(url).then(function(data){
  					$scope.mediumSelection.push(data);
  				})
  			})
  		});
  	}
  	/**
  	* @function deleteArtwork
  	* @description: Delete artwork object Wrapper function for UI
  	* ToDo: Add Model with Question before deleting an object
  	**/
  	$scope.deleteArtwork = function(artwork){
  		var delObj = artwork;
  		Artwork.deleteArtwork(artwork.url).then(function(data){
  			//Success 
  			//ToDo: Return a visual message
  			//Delete object from list
  			$scope.artworks.forEach(function(elem, index, array){
  				if(elem.name === delObj.name){
  					$scope.artworks.remove(index);
  				}
  			});
  			$scope.cacheArtworks.forEach(function(elem, index, array){
  				if(elem.name === delObj.name){
  					$scope.artworks.remove(index);
  				}
  			});
  		});
  	}

  	/**
  	* Create new artwork
  	**/
  	$scope.createArtwork = function(artwork){
  		//console.log(artwork);
  		artwork.medium = $scope.selectedMedium.element.url;
  		if($scope.selectedUnit.element.name === "cm"){
  			artwork.dimensions_in_cm = true;
  		}
  		artwork.year = parseInt(artwork.year);
  		artwork.price = parseInt(artwork.price);
  		artwork.dimension1 = parseInt(artwork.dimension1);
  		artwork.dimension2 = parseInt(artwork.dimension2);
  		artwork.dimension3 = parseInt(artwork.dimension3);

  		if(artwork.includes_vat){
  			artwork.vat = parseInt(artwork.vat);
  		}else{
  			artwork.vat = null;
  		}
  		Artwork.createArtwork(artwork).then(function(success){
  			$scope.isNewArtwork = false;
  			  	$scope.artwork = {
			  		"artist": "",
			  		"title": "",
			  		"year": "",
			  		"price": "",
			  		"description": "",
			  		"includes_vat": "",
			  		"vat": "",
			  		"dimension1":"",
			  		"dimension2":"",
			  		"dimension3":"",
			  		"dimensions_in_cm": false,
			  	};
			limitElements = 10;
  			lastIndex = 0;
  			$scope.cacheArtworks = {};
  			$scope.artworks = new Array();
			$scope.reloadData();
  		});
  	}

  	$scope.reloadData = function(){
  		  	//Get list of all artworks
  	Artwork.getArtworks().then(function(artworks){
  		$scope.cacheArtworks = artworks;
  		//API returns only HTTP-Endpoints for Details -> Iterate over array and retrieve list
  		//To improve speed loading only a first bunch of data
  		var i = lastIndex;
  		//There could be less artworks in the DB than our limit
  		if(limitElements>= $scope.cacheArtworks.urls.length){
  			limitElements = $scope.cacheArtworks.urls.length;
  		}
  		for(;i < limitElements;i++){
  			var url = $scope.cacheArtworks.urls[i];
  			Artwork.getArtwork(url).then(function(elem){
  				var artwork = elem;
  				artwork.materialList = new Array();
  				//Retrieve Array with HTTP Endpoints for Materials
  				//After that request information for each Material
  				var promMaterial =Material.getMaterials(artwork.materials).then(function(materialsArray){
  					materialsArray.urls.forEach(function(url, index, array){
  						Material.getMaterial(url).then(function(material){
  							artwork.materialList.push(material);
  						});
  					});
  				});
  				//After all materials are returned
  				//Get Medium
  				promMaterial.then(function(){
  					Medium.getMedium(artwork.medium).then(function(medium){
  						artwork.mediumList = medium;
  							$scope.artworks.push(artwork);
  					});
  				});
  			});
  		}
  		lastIndex = i;
  	});
  	}

 }]);
})();