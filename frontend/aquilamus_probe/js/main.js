/**
 * Setup of main AngularJS application.
 *
 * @see controllers
 * @see services
 * @see directives
 */
(function(){
	'use strict';

	var demoApp = angular.module('demoApp', ["demoAppControllers", "demoAppServices","demoApp.dropdown"]);
})();



