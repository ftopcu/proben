/**
 * Services for retrieving different objects
 *
 * @see Artwork
 * @see Material
 * @see Medium
 */

(function(){
'use strict';

var demoAppServices = angular.module('demoAppServices', ['ngResource']);

//Factory for Handling API-Calls to Artwork API
demoAppServices.factory('Artwork', ['$resource','$http','$q','$rootScope',
		function( $resource, $http, $q, $rootScope){

			var host = "http://54.77.217.175";
			$http.defaults.headers.post['Content-Type'] = 'application/json';
		    $http.defaults.useXDomain = true;

			//Basic "logger" of API errors
			//Current: Just simply console outputs
			function apiErrorHandler(err, status, promise){
				console.error("An erorr occurred during data retrieving.", err, status);
				promise.reject(err);
			}

			//Retrieving artworks list
			//Working with promise to allow async function calls in controllers
			function getArtworks(){
				var deferred = $q.defer();
				$http({method: "GET", url: host + "/artworks"}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			//Input: Artwork URL
			//Returns: JSON Object with details about the artwork
			function getArtworkDetails(artwork_url){
				var deferred = $q.defer();
				$http({method: "GET", url: artwork_url}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			function deleteArtwork(artwork_url){
				var deferred = $q.defer();
				$http({method: "DELETE", url: artwork_url}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			function createArtwork(json){
				var deferred = $q.defer();
				$http({method: "POST", data:json,  url: "http://54.77.217.175/artworks"}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			return{
				/**
	             * @function getArtworks
	             * @returns a HTTP promise that eventually resolves with a list of artworks
	             */
				getArtworks: getArtworks,
				/**
	             * @function getArtwork
	             * @returns a HTTP Promise that eventually resolves with information about a given artwork
	             */
				getArtwork: getArtworkDetails,
				/**
	             * @function deleteArtwork
	             * @returns a Promise that eventually resolves with a success message about the deleted artwork
	             */
				deleteArtwork: deleteArtwork,
				/**
	             * @function createArtwork
	             * @returns a HTTP Promise that eventually resolves with a success message about the created artwork
	             */
				createArtwork: createArtwork
			}
}]);

demoAppServices.factory('Medium', ['$resource','$http','$q','$rootScope',
		function( $resource, $http, $q, $rootScope){
			var host = "http://54.77.217.175";

			//Basic "logger" of API errors
			//Current: Just simply console outputs
			function apiErrorHandler(err, status, promise){
				console.error("An erorr occurred during data retrieving.", err, status);
				promise.reject(err);
			}

			function getMediumForArtwork(medium_url){
				var deferred = $q.defer();
				$http({method: "GET", url: medium_url}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			function getMediums(){
				var deferred = $q.defer();
				$http({method: "GET", url: host + "/mediums"}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			return{
				/**
	             * @function getMedium
	             * @returns a HTTP Promise that eventually resolves with information about a given medium
	             */
				getMedium: getMediumForArtwork,
				/**
	             * @function getMediums
	             * @returns a HTTP Promise that eventually resolves to the list of mediums
	             */
				getMediums: getMediums
			}
}]);

demoAppServices.factory('Material', ['$resource','$http','$q','$rootScope',
		function( $resource, $http, $q, $rootScope){
			var host = "http://54.77.217.175";

			//Basic "logger" of API errors
			//Current: Just simply console outputs
			function apiErrorHandler(err, status, promise){
				console.error("An erorr occurred during data retrieving.", err, status);
				promise.reject(err);
			}

			function getMaterialForArtwork(material_url){
				var deferred = $q.defer();
				$http({method: "GET", url: material_url}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			function getDetailForMaterial(material_url){
				var deferred = $q.defer();
				$http({method: "GET", url: material_url}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			function deleteMaterial(material_url){
				var deferred = $q.defer();
				$http({method: "DELETE", url: material_url}).
				success(function(data, status, headers, config){
					deferred.resolve(data);
				}).
				error(function(err, status, headers, config){
					//Error Handling 
					apiErrorHandler(err, status);
				});
				return deferred.promise;
			}

			return{
				/**
				*	@function getMaterials
				*	@returns a HTTP Promise that eventually resolves to the list of materials
				*/
				getMaterials: getMaterialForArtwork,
				/**
				* @function getMaterial
				* @returns a HTTP Promise that eventually resolves with a material (which was given before)
				*/
				getMaterial: getDetailForMaterial,
				/**
	             * @function deleteMaterial
	             * @returns a HTTP Promise that eventually resolves with a success message about the delete status of a given material url
	             */
				deleteMaterial: deleteMaterial
			}
}]);

})();