(function(multithek, $, moment) {
    'use strict';
    //init momentjs (german)
    moment.locale('de');
    multithek.epg = {

        // will be set on page setup; used to trigger some elements to be fixed after passing this scroll position
        filterNavPosTop: 0,
        // flag to reduce function calls
        isFixed: false,

        $nowBar: null,

        channelIterator: 0, //initial value,
        channelLimiter: 4,  //We are only displaying 4 channels each time, default: 4
        channelsCounter: 0,  //the sum of all channels
        lastNavigationAction: 'initEpg', //saves last Navigation action, default: "initEpg", | "getNextEpg", "getPreviousEpg", "endOfNextEpg"
        epgIsLoading: false, //state of epg Loading, necessary to prevent click hijacking
        epgList: [], //global variable for saving currently used epglist (all or filtered)
        init: function() {
            if($('.epg').length > 0){
                this.setListeners();

                this.addHalfHourLines();
                this.setNowBar();

                var body = $("html, body");
                body.animate({scrollTop:this.$nowBar.position().top - 200}, '500', 'swing',function(){
                    console.log('scroll complete');
                    multithek.lazy.setLazyLoad();
                });
            }
        },

        setListeners: function() {
            this.setRegions();
            this.setTvShowClickListener();
            this.setTvShowRatingClickListener();
            this.setFavIconClickListener();
            this.setFavIconFilterClickListener();
            this.setStickyStateListener();
            //this.setDropDownListeners(); -> will be called later, as we have to request Regionlist before
            this.setEPGNavigationListener();
            //initialize
            this.initPaging(4);
            this.initEpg();
        },
        /*
        ** @function initPaging -  initializes the limiter for paging
        ** @input:
        **      limiter - an integer value which sets the limiter for each paging 
        */
        initPaging: function(limiter){
            if(typeof limiter != "undefined"){
                this.channelIterator = 0; //start for the Iterator
                this.channelLimiter = limiter; //set limiter
                this.channelsCounter = 0; //helper Value to know how much channels we have
            }else{
                //Limiter is set to the default value: 4
                this.channelIterator = 0; //should init all important values
                this.channelsCounter = 0;
            }
        },
        /*
        ** @function initEpg -  initializes the epg (header and basic navigation settings) 
        ** should be used for initializing the epg when the users moves
        */
        initEpg: function(){
            var self = this;
            //Request channel list (all)
            multithek.fraunhofer.setEpgList(function(epglist){
                self.setEpg(epglist);
            });
        },
        /*
        ** @function setEpg -  helper method.
        ** Initializes the View, clears all old entries and starts the initialization process
        ** @input:
        **      epglist - channellist to initialize the view and paging
        */
        setEpg: function(epglist){
            //Clear channel Headers List
            $('.channelHeaders').empty();
            //Set epgList 
            this.epgList = epglist;
            this.channelsCounter = epglist.length;
            //create initial EPG View
            this.setEpgChannels(this.epgList, this.channelIterator, this.channelLimiter, this.channelLimiter, function(){});
            //initially our Paging History starts with "initEpg";
            this.lastNavigationAction = "initEpg";
            $('.navRight').removeClass('disabled');
            $('.navLeft').addClass('disabled');
        },
        /*
        ** @function getNextEpg -  loads next channels in the channel list based on the current state of the channelIterator and the channelLimiter
        ** Initializes the View, clears all old entries and starts the initialization process
        **  @input:
        **          successCallback - is called after the next channels and detail informations are loaded
        */
        getNextEpg: function(successCallback){
            var self = this;
            var epglist = self.epgList;
            //Check: Iterator is not null or minus, and the channelIterator is not at the end of the list (<= length of epglist)
            if(self.channelIterator >= 0 && self.channelIterator <= epglist.length) {
                //clear channel Headers
                $('.channelHeaders').empty();
                //Check: If we iterate to the next channels, we are not reaching the end of the list
                if((self.channelIterator + self.channelLimiter) < epglist.length){
                    //Check: Last Action was "getNextEpg" or we start at the beginning
                    if(self.lastNavigationAction == 'getNextEpg' || self.lastNavigationAction == 'initEpg'){ 
                        //In this case, start from channelIterator's position, go to channelIterator+channelLimiter and update the Iterator with channelLimiter
                        self.setEpgChannels(epglist, self.channelIterator, self.channelIterator+self.channelLimiter, self.channelLimiter, successCallback);
                    }else{ 
                        //last Action was "getPreviousEpg" -> We need to change the slice limits
                        //In this case, start: channelIterator+channelLimiter, end at:  channelIterator+channelLimiter*2 and update the Iterator with channelLimiter*2
                        //we are not adding channelLimiter*2 as an arguments because setEpgChannels is using the start point and adds the Limiter -> for more details 
                        //read the explanations for setEpgChannels!
                        self.setEpgChannels(epglist, self.channelIterator+self.channelLimiter, self.channelIterator+self.channelLimiter*2, self.channelLimiter, successCallback);
                    }
                    //We are now able to navigate back / left
                    $('.navLeft').removeClass('disabled');
                    self.lastNavigationAction = 'getNextEpg';//set navigationHistory
                }else{
                    //We are at the end of our channel list 
                    if((self.channelIterator + self.channelLimiter) >= epglist.length){
                        if(self.lastNavigationAction == 'getNextEpg' || self.lastNavigationAction == 'initEpg'){ //Last Action was "getNextEpg" or we start at the beginning
                            //In this case, start from channelIterator's position, go to epglist.length and don't update the Iterator -> would break the paging
                            self.setEpgChannels(epglist, self.channelIterator,epglist.length,0, successCallback);
                        }else{
                            //Last Acton was getPreviousEpg (we were at the end, clicked one back and are now again interested in the last channels)
                            //In this case, start from channelIterator+channelLimiter, go to epglist.length and don't update the Iterator 
                            self.setEpgChannels(epglist, self.channelIterator+self.channelLimiter,epglist.length,0, successCallback);
                        }
                        //navRight disabled
                        $('.navRight').addClass('disabled');
                        //For the case that we only have 2 pages of channels
                        $('.navLeft').removeClass('disabled');
                        self.lastNavigationAction = 'endOfNextEpg'; //set navigationHistory
                    }
                }
                //Animate the user to the current time epg position
                var body = $("html, body");
                body.animate({scrollTop:this.$nowBar.position().top - 200}, '500', 'swing',function(){
                    console.log('scroll complete');
                });
            }
        },
        /*
        ** @function getPreviousEpg -  loads next channels in the channel list based on the current state of the channelIterator and the channelLimiter
        ** Initializes the View, clears all old entries and starts the initialization process
        **  @input:
        **          successCallback - is called after the next channels and detail informations are loaded
        */
        getPreviousEpg: function(successCallback){
            var self = this;
            var epglist = self.epgList;
            //Check: Iterator is not null or minus, and the channelIterator is not at the end of the list (<= length of epglist)
            if(self.channelIterator >= 0 && self.channelIterator <= epglist.length) {
                //clear channel Headers
                $('.channelHeaders').empty();
                //Check: we don't have reached the beginning of our list
                if((self.channelIterator - self.channelLimiter) > 0){
                    //Last Action was getPreviousEpg
                    if(self.lastNavigationAction == "getPreviousEpg" || self.lastNavigationAction == "endOfNextEpg"){
                        //In this case, start from channelIterator-channelLimiter, go to channelIterator and update the Iterator  (-channelLimiter)
                        self.setEpgChannels(epglist,self.channelIterator - self.channelLimiter, self.channelIterator, 0, successCallback); 
                    }else{
                        //In this case, start from channelIterator-channelLimiter*2, go to self.channelIterator - self.channelLimiter
                        self.setEpgChannels(epglist, self.channelIterator - self.channelLimiter*2, self.channelIterator - self.channelLimiter, 0, successCallback);
                        if((self.channelIterator - self.channelLimiter) <= 0){
                            //If we  only go one page forward and than backward again, the need to check this
                            $('.navLeft').addClass('disabled');
                            self.channelIterator = self.channelLimiter; //reset to the channelLimiter value, as we are only able to load next channels and don't want to start from 0 again
                            self.lastNavigationAction = "initEpg"; //set navigationHistory -> if we are at the beginning the action should be "initepg"
                        }
                    }
                    $('.navRight').removeClass('disabled');
                    self.lastNavigationAction = "getPreviousEpg"; //set navigationHistory
                }else{
                    //Check: we are at the beginning of our list
                    if((self.channelIterator - self.channelLimiter) <= 0){
                        self.setEpgChannels(epglist, 0, self.channelIterator, 0, successCallback);
                        $('.navLeft').addClass('disabled');
                        $('.navRight').removeClass('disabled');
                        //reset to the channelLimiter value, as we are only able to load next channels and don't want to start from 0 again
                        self.channelIterator = self.channelLimiter; 
                        //set navigationHistory -> if we are at the beginning the action should be "initepg"
                        self.lastNavigationAction = "initEpg"; 
                    }
                }
                //Animate the user to the current time epg position
                var body = $("html, body");
                body.animate({scrollTop:this.$nowBar.position().top - 200}, '500', 'swing',function(){
                    console.log('scroll complete');
                });
            }
        },/*
        ** @function setEpgChannels -  loads channels, initializes the new header
        **  @input:
        **          epglist - the epglist with which the navigation is currently iterating
        **          start - start point to slice the epglist (and additionally is used to update channelIterators new value)
        **          end - slice end point for epglist
        **          iterateValue - iteration value for channelIterator (at the end the channelIterator = start + iterateValue)
        **          cb - success Callback
        */
        setEpgChannels: function(epglist, start, end, iterateValue, cb){
            var self = this;
            self.channelsCounter = epglist.length;
            //Slice epglist to retrieve channels
            var epgForFour = epglist.slice(start,end);
            //iterate over the channels and append Channel Information to the DOM
            epgForFour.forEach(function(element, index, array){
                var domElement = '<div class="programHeader" data-channelid="'+element.id+'"><div class="programType">'+element.channel_type+'</div><div class="programName">'+element.title+'</div><a href="" class="favIcon"></a></div>';
                $('.channelHeaders').append(domElement);
                self.setFavState(element.id);
            });
            $('.channelHeaders').append('<div style="clear: both"></div>');
            //set FavIcon Listeners again
            self.setFavIconClickListener();
            //set Epg Programs -> request tv Show basic information for the channels
            self.addEpgPrograms(epgForFour);
            //update channelIterator
            this.channelIterator = start + iterateValue;
            cb();
        },
        /*
        ** @function setFavState -  sets new favourite state for a given channel
        **  @input:
        **          channelid - is called after the next channels and detail informations are loaded
        */
        setFavState: function(channelid){
            if(channelid != null){
                var $parent = $('div[data-channelid="'+channelid+'"]'),
                    favIcon = $parent.find('.favIcon');
                multithek.fraunhofer.checkFavoriteState(channelid, function(){
                    if(!favIcon.hasClass('active')){
                        favIcon.addClass('active');
                    }
                });
            }
        },
        /*
        ** @function addEpgPrograms - helper method which requests epg program information for given epglist
        **  @input:
        **          epglist - a list of channels for which tv Show details should be requested
        */
        addEpgPrograms: function(epglist){
            var self = this;
            //Request basic tv Show information
            multithek.fraunhofer.setEpgPrograms(epglist, function(programsList){
                //creates DOM element for each tv show (program)
                self.setEpgProgramsView(programsList);
                self.addHalfHourLines();
                self.setNowBar();
                //Re-init some listeners
                multithek.epg.setTvShowClickListener();
                multithek.epg.setTvShowRatingClickListener();
            });
        },
        /*
        ** @function setEpgProgramsView - helper method which creates DOM elements for given list of tv shows
        ** Additionally, after adding elements to the DOM it requests asynchronously detail Program Information for the detail View
        **  @input:
        **          programsList - a list of programs
        */
        setEpgProgramsView: function(programsList){
            var self = this;
            //clear channels div
            $('.channels').empty();
            var keyCount = 0;
            //Iterate via channels, which are represented as Key-value pairs in the JSON object
            for(var key in programsList.grid.channels){
                //create epg entry
                //We have four scheme Div's -> therefore our keyCount iterates from 0 to 3
                var programParent = '<div class="programColumn scheme-'+(keyCount+1)+'"></div>';
                //append programParent to channels Div (div.channels is the overall parent element)
                $('.channels').append(programParent);
                //iterate over epg programs for each channel (key)
                programsList.grid.channels[key].forEach(function(element, index, array){
                    //calculate duration in minutes to add div-height
                    var timediff = (element.end - element.begin)/60;
                    //Iterate over each entry and add program entry
                    var timeString = multithek.fraunhofer.getPrettyTime(element.begin);
                    //load detailed information of program
                     //ToDo: Extend Lazy Loading to Call this Information also asynchronously based on user scrolling
                    self.loadProgramInfoAsync(element.id);
                    var tvShowEntry = '<div class="tvShow m'+timediff+'" data-showid="'+element.id+'"><div class="showWrapper"><div id="'+element.id+'" class="image"></div><span class="showTime">'+timeString+'</span><div class="showTitle">'+element.title+'</div></div></div>';
                    var selectElement = '.scheme-'+(keyCount+1); //Let's create specific scheme-x selector to select correct channel <-> epg match
                    $(selectElement).append(tvShowEntry);
                });
                keyCount++;
            }
            //Add halfHourIndicators and nowBar
            $('.channels').append('<div style="clear: both"></div><div class="halfHourIndicators"></div><div class="nowBar">Jetzt</div>');
        },
        /*
        ** @function loadProgramInfoAsync - helper method which requests tv Show information
        **  @input:
        **          slotid - id of tv show
        */
        loadProgramInfoAsync: function(slotid){
            var self = this;
            //Load data for specific TV show 
            multithek.fraunhofer.getProgramDetail(slotid, function(json){
                self.setProgramInfoDetail(slotid, json);
            });
            return;
        },
        /*
        ** @function setProgramInfoDetail - helper method which updates DOM element of a tv Show for the detail view
        **  @input:
        **          slotid - id of tv show
        **          json - json representation (as response from server) of tv show
        */
        setProgramInfoDetail: function(slotid, json){
            var data = json;
            //update element based on slotid
            var tvShowParent = $('div[data-showid="'+slotid+'"]');
            //Update image
            if(data[slotid].image_url === '' || typeof data[slotid].image_url == 'undefined' || data[slotid].image_url === null){
                //fallback if there is no image given
                tvShowParent.find('.image').attr('data-src', 'img/showImage.jpg');
            }else{
                tvShowParent.find('.image').attr('data-src', data[slotid].image_url);
            }
            //Add Rating
            //First remove "possible" old entries
            tvShowParent.find('.showWrapper .showRating').remove();
            tvShowParent.find('.showWrapper').append('<ul class="showRating rating'+data[slotid].Rating+'" data-rating="'+data[slotid].Rating+'"><li></li><li></li><li></li><li></li><li></li></ul>');
            //Add Description Text
            tvShowParent.find('.showWrapper .showDetails').remove();
            tvShowParent.find('.showWrapper').append('<div class="showDetails">'+data[slotid].description.slice(0, 250)+'</div>');
            //Add More Button
            tvShowParent.find('.showWrapper .more').remove();
            tvShowParent.find('.showWrapper').append('<a class="more" href="/multithek/tvShow-detail.html#'+slotid+'">» Mehr</a>');
            //Add Share Button
            tvShowParent.find('.showWrapper .share').remove();
            tvShowParent.find('.showWrapper').append('<a class="share" href="">» Share</a>');
            //setTVRatingListener
            this.setTvShowRatingClickListenerForSlot(slotid);
            //init lazy loading script
            multithek.lazy.init();
        },
        /*
        ** @function setEPGNavigationListener - initalizes Listeners for paging
        */
        setEPGNavigationListener: function(){
            var self = this;
            $('.navRight').on('click',function(e){
                e.preventDefault();
                if(!$(this).hasClass('disabled')){
                    //prevent click hijacking
                    $(this).addClass('disabled');
                    var element = this;
                    var event = e;
                    self.getNextEpg(function(){
                        //allow new action after epg is loaded successfully
                        $(element).removeClass('disabled');
                    });
                }
            });

            $('.navLeft').on('click',function(e){
                e.preventDefault();
                if(!$(this).hasClass('disabled')){
                    //prevent click hijacking
                    $(this).addClass('disabled');
                    var element = this;
                    var event = e;
                    self.getPreviousEpg(function(){
                        //allow new action after epg is loaded successfully
                        $(element).removeClass('disabled');
                    });
                }
            });
        },
        // add the indicator lines for every 30 minutes
        addHalfHourLines: function() {
            var hour = 0,
                labels = '';
            do{
                labels += '<div class="halfHourLine" style="top:' + (hour * 60 + 15) + 'rem"><span>' + (hour + 5 ) % 24 + ':00</span></div>';
                labels += '<div class="halfHourLine" style="top:' + (hour * 60 + 45)+ 'rem"><span>' + (hour + 5 ) % 24 + ':30</span></div>';
            }while(++hour < 24);

            $('.halfHourIndicators').append(labels);
        },

        // set the red 'now' line to the current time position and move it along the epg every minute
        setNowBar: function(){
            this.$nowBar = $('.nowBar');
            var callback = function(){
                var now = new Date();
                multithek.epg.$nowBar.css('top', (now.getHours()* 60 + now.getMinutes() + 15) + 'rem');
            };
            callback();
            setInterval(callback,60000);
        },
        /*
        ** @function setRegions - requests List of regions and adds response to navigation
        */
        setRegions: function(){
            var self = this;
            var parent= $('.channelRegions .options');
            multithek.fraunhofer.getRegions(function(regions){
                //Iterate over keys, return is a JSON Object not an array!
                Object.keys(regions).forEach(function(id, index){//iterate over keys in object
                    var capitalize = regions[id][0].toUpperCase() + regions[id].slice(1);
                    var regionString = '<a href="#'+regions[id]+'" class="indent">'+capitalize+'</a>';
                    parent.append(regionString);
                });
                //init DropDownListener here
                self.setDropDownListeners();
            });
        },
        /**************************
        * Set Listeners
        ***************************/
        checkForCurrentRunningStreamShows: function(){
        //    console.log(multithek.epg.$nowBar.position());
            var barTop = multithek.epg.$nowBar.position().top;
                $channels = $('.programColumn'),
                $channel = null,
                $shows = null,
                barValue = 0;
            $channels.each(function(i){
                $channel =  $(this)
                $shows = $channel.find('.tvShow');
                $shows.removeClass('playNow');
                $shows.each(function(){
                    barValue = barTop - $channel.position().top;
                    if($(this).position().top < barValue && $(this).position().top + $(this).height() + 10 > barValue){
                        $(this).addClass('playNow');
                    }
                });
            });
        }, 
        setStickyStateListener: function() {
            // set the threshold for triggering the elements being fixed
            this.filterNavPosTop = $('.epg').offset().top;
            $(window).on('scroll', function(){
                var top = $(window).scrollTop();
                if(top > multithek.epg.filterNavPosTop && !multithek.epg.isFixed){
                    $('.willBeFixed').addClass('epg-fixed');
                    $('.channels').addClass('fixed-layout');
                    multithek.epg.isFixed = true;
                }else if(top <= multithek.epg.filterNavPosTop && multithek.epg.isFixed){
                    $('.willBeFixed').removeClass('epg-fixed');
                    $('.channels').removeClass('fixed-layout');
                    multithek.epg.isFixed = false;
                }
            });
        },

        // clicking on a tv show will expand to detail view and close all other detail views of that channel
        setTvShowClickListener: function() {
            $('.tvShow').on('click',function(e){
                //as long as we are not clicking on the "Mehr"/"Share" Button, the tvShow should be toggled
                if(e.target.className != 'more' && e.target.className != 'share'){
                    //toggle Detail View
                    e.preventDefault();
                    var slotid = $(this).attr('data-showid');
                    var $tvShow = $(this);
                    if($tvShow.hasClass('active')){
                        $tvShow.removeClass('active');
                    }else{
                        // activate clicked on show and hide other shows' details of channel
                        $tvShow.addClass('active').siblings().removeClass('active');
                    }
                    return false;
                }else{
                    //do nothing
                }
            });
        },

        // tv shows have a rating system and a click on it triggers a server request
        setTvShowRatingClickListener: function() {

            $('.showRating li').on('click',function(e){
                e.preventDefault();

                var $tvShowRating = $(this),
                    showId = $tvShowRating.closest('.tvShow').attr('data-showId'),
                    state = $tvShowRating.index(),
                    callback = function(){
                        var $ratingContainer = $tvShowRating.parent(),
                            rating = $ratingContainer.attr('data-rating');

                        $ratingContainer.removeClass('rating' + rating);
                        $ratingContainer.addClass('rating' + (state + 1));
                        $ratingContainer.attr('data-rating', state +1);
                    };

                // send request to server
                multithek.fraunhofer.setRatingForShow(showId, state, callback);

                return false;
            });
        },
        // tv shows have a rating system and a click on it triggers a server request
        setTvShowRatingClickListenerForSlot: function(slotid) {
            var selectElement = 'div[data-showid="'+slotid+'"] .showRating li';
            $(selectElement).on('click',function(e){
                e.preventDefault();

                var $tvShowRating = $(this),
                    showId = $tvShowRating.closest('.tvShow').attr('data-showId'),
                    state = $tvShowRating.index(),
                    callback = function(){
                        var $ratingContainer = $tvShowRating.parent(),
                            rating = $ratingContainer.attr('data-rating');

                        $ratingContainer.removeClass('rating' + rating);
                        $ratingContainer.addClass('rating' + (state + 1));
                        $ratingContainer.attr('data-rating', state +1);
                    };

                // send request to server
                multithek.fraunhofer.setRatingForShow(showId, state, callback);

                return false;
            });
        },

        /*
        ** @function setFavIconClickListener - updates favourite status of channel 
        */
        setFavIconClickListener: function() {
            $('.favIcon').on('click',function(e){
                e.preventDefault();

                var $button = $(this),
                    channelId = $button.parent().attr("data-channelid"),
                    state = $button.hasClass('active'),
                    callback = function(){
                        $button.toggleClass('active');

                    };
                // send request to server
                multithek.fraunhofer.setFavouriteState(channelId, state, callback);

                return false;
            });
        },
        /*
        ** @function setFavIconFilterClickListener - listeners for showing only favoured channels
        */
        setFavIconFilterClickListener: function() {
            var self = this;
            $('.onlyFavourites').on('click',function(e){
                e.preventDefault();

                var $button = $(this),
                    //the method showOnlyFavourites needs the state "true" to load the customized epg list
                    //checking the class "active" will result in "false" which is than redirected to the method and not executed
                    state = !($button.hasClass('active')),
                    callback = function(channellist){
                        if(channelllist.hasOwnProperty("length") && channellist.length > 0){
                            //At least we should have one channel in the list
                            $button.toggleClass('active');
                            //Clear Channel Headers
                            $('.channelHeaders').empty();
                            //Reset all paging variables
                            self.channelsCounter = channellist.length;
                            self.epgList = channellist;
                            //init all counters
                            self.channelIterator = 0;
                            //Load first set of EPG Channels
                            self.setEpgChannels(self.epgList, self.channelIterator, self.channelLimiter, self.channelLimiter, function(){
                                if(channellist.length <= self.channelIterator){
                                    $('.navRight').addClass('disabled');
                                    $('.navLeft').addClass('disabled');
                                }else{
                                    //we are beginning from the beginning, so there is no navigation to the left
                                    $('.navLeft').addClass('disabled');
                                }
                            });
                            self.lastNavigationAction = "initEpg";
                        }else{
                            //ToDo: What to do if the Favourite list is empty ??
                        }
                    };
                if(state){
                    //show only favoured channels
                    // send request to server
                    multithek.fraunhofer.showOnlyFavourites(state, callback);
                }else{
                    //reset view as the user clicked on the icon again
                    $button.toggleClass('active');
                    self.initPaging(self.channelLimiter);//re-init epg
                    self.initEpg();
                }
                return false;
            });
        },

        setDropDownListeners: function() {
            var self = this;
            $('.channelRecommendations').find('a').on('click',function(e){
                e.preventDefault();

                var $button =$(this),
                    state = $button.attr('href').substr(1) === 'all',
                    callback = function(channellist){
                        $button.closest('.dropdown').find('span').html($button.text());
                        $button.addClass('active').siblings().removeClass('active');
                        if(channellist.length > 0){
                            //init epg
                            self.initPaging(self.channelLimiter);
                            self.setEpg(channellist);
                            if(channellist.length <= self.channelIterator){
                                $('.navRight').addClass('disabled');
                                $('.navLeft').addClass('disabled');
                            }else{
                                //we are beginning from the beginning, so there is no navigation to the left
                                $('.navLeft').addClass('disabled');
                            }
                        }else{
                            //ToDo: How to visualize an empty channel list ?
                        }
                    };
                // only trigger if button was not already active
                if(!$button.hasClass('active')){
                    multithek.fraunhofer.setChannelRecommendation(state, callback);
                }
                return false;
            });
            $('.channelSources').find('a').on('click',function(e){
                e.preventDefault();
                var $button =$(this),
                    state = $button.attr('href').substr(1),
                    callback = function(channellist){
                        $button.closest('.dropdown').find('span').html($button.text());
                        $button.addClass('active').siblings().removeClass('active');
                        if(channellist.length > 0){
                            //init epg
                            self.initPaging(self.channelLimiter);
                            self.setEpg(channellist);
                            if(channellist.length <= self.channelIterator){
                                $('.navRight').addClass('disabled');
                                $('.navLeft').addClass('disabled');
                            }else{
                                //we are beginning from the beginning, so there is no navigation to the left
                                $('.navLeft').addClass('disabled');
                            }
                        }else{
                            //ToDo: How to visualize an empty channel list ?
                        }
                    };
                // only trigger if button was not already active
                if(!$button.hasClass('active')) {
                    multithek.fraunhofer.setChannelSource(state, callback);
                }
                return false;
            });
            $('.channelRegions').find('a').on('click',function(e){
                e.preventDefault();
                var $button =$(this),
                    state = $button.attr('href').substr(1),
                    callback = function(channellist){
                        $button.closest('.dropdown').find('span').html($button.text());
                        $button.addClass('active').siblings().removeClass('active');
                        if(channellist.length > 0){
                            //init epg
                            self.initPaging(self.channelLimiter);
                            self.setEpg(channellist);
                            if(channellist.length <= self.channelIterator){
                                $('.navRight').addClass('disabled');
                                $('.navLeft').addClass('disabled');
                            }else{
                                //we are beginning from the beginning, so there is no navigation to the left
                                $('.navLeft').addClass('disabled');
                            }
                        }else{
                            //ToDo: How to visualize an empty channel list ?
                        }

                    };
                // only trigger if button was not already active and is no region group button
                if(state) {
                    multithek.fraunhofer.setChannelRegion(state, callback);
                }
                return false;
            });
        },
        /*
        ** @function initDetailView - initializes the Detail view for a tv show
        */
        initDetailView: function(){
            //get slotid via url hash
            var slotid = decodeURIComponent(window.location.hash.slice(1));
            var self = this;
            //We need to load the channel list -> the recommender needs a list of channels -> default: all channels
             multithek.fraunhofer.setEpgList(function(epglist){
                //for sake of completeness, init epglist and paging
                self.epgList = epglist;
                self.channelsCounter = epglist.length;
                //load tv Show Detail data
                multithek.fraunhofer.getProgramDetail(slotid, function(json){
                    var data = json;
                    //get parent element to append tv show info
                    var tvShowParent = $('.single-tv-show');
                    //init tv show element
                    var tvShowElement = $('<div class="showInfo"><div class="preview"></div></div>');
                    //add tv show image
                    //Check: if there is no tv-show image given
                    if(data[slotid].image_url === '' || typeof data[slotid].image_url == 'undefined' || data[slotid].image_url === null){
                        tvShowElement.find('.preview').append('<img src="img/showImage.jpg" alt="" /><div class="previewPlay"></div>');
                    }else{
                        tvShowElement.find('.preview').append('<img src="'+data[slotid].image_url+'" alt="" /><div class="previewPlay"></div>');
                    }
                    //Add tv show title
                    tvShowElement.append('<h1>'+data[slotid].title+'</h1>');
                    //Check if there is a subtitle (not given for every tv show)
                    if(data[slotid].subtitle !== null){
                        tvShowElement.append('<h2>'+data[slotid].subtitle+'</h2>');
                    }else{
                        tvShowElement.append('<h2></h2>');
                    }
                    //Add Description
                    //The slotid consists of the channel Name and timestamp of tv show
                    var desc = slotid.split(';'); // 0: Channel Name , 1: Timestamp
                    var timeFormat = 'MM-DD-YYYY HH:mm:ss';
                    var start = moment(data[slotid].begin);
                    var end = moment(data[slotid].end);
                    //Add description part to tv show
                    tvShowElement.append('<div class="description"><h3>'+desc[0]+' - '+start.format("LLLL")+' Uhr ('+end.diff(start, 'minutes')+' Min.)</h3><p>'+data[slotid].description+'</p></div>');
                    //create details dom element
                    var detailsHtmlString = $('<div class="details"></div>');
                    //Add Rating to details 
                    detailsHtmlString.append('<ul class="showRating rating'+data[slotid].Rating+'" data-rating="'+data[slotid].Rating+'"><li></li><li></li><li></li><li></li><li></li></ul>');
                    //Add timing information to details
                    detailsHtmlString.append('<span></span><p><strong>'+desc[0]+'</strong><br/>'+moment(data[slotid].begin).format("HH:mm")+' - '+moment(data[slotid].end).format("HH:mm")+' - '+data[slotid].length+' Min.</p><ul class="additionalInformation"></ul>');
                    // Add Play Button and set video url
                    detailsHtmlString.append('<p>'+data[slotid].info+'</p> <div style="clear: both"></div>');
                    //Add Type
                    tvShowElement.append(detailsHtmlString);
                    //Append all to parent
                    tvShowParent.append(tvShowElement);
                    //init aside container
                    tvShowParent.append('<aside><div class="teaser twitterFeed"><h3 class="icon-title icon-twitter">Twitter Feed</h3></div><div class="teaser recommendations"><h3 class="icon-title icon-recommendation">Empfohlene Sendungen</h3></div><button id="playIPStream">Play</button><div id="videodiv" style="display:none"></div></aside>');
                    /*
                    ** init of video player
                    ** the view (DOM) should have at least "videodiv" div element which is not displayed (display:none)
                    ** if the user clicks on the button, the player will be initialized
                    */
                    multithek.fraunhofer.getStreamUrls(self.epgList, desc[0], function(urls){
                        var streamUrls = [];
                        if(Array.isArray(urls)){
                            //For possible future change of stream_url entries
                            streamUrls = urls;
                        }else{//urls is one object
                            //only one element is given
                            //for the current situation
                            if(urls != null){
                                streamUrls.push({src: urls, type:"application/x-mpegurl"});
                            }else{
                                //ToDo: Only for testing purpose
                                streamUrls.push({src: "http://c40000-l.u.core.cdn.streamfarm.net/40000mb/live/4012hbbtv/bloomberg.isml/playlist.m3u8", type:"application/x-mpegurl"});
                            }
                        }
                        //init flash options
                        videojs.options.flash.swf = "js/player/video-js_hls.swf";
                        document.getElementById('playIPStream').addEventListener("click", function() {
                            document.getElementById("videodiv").style.display ="block";
                            multithek.fraunhofer.initVideo(document.getElementById("videodiv"),streamUrls);
                        });
                    });
                    /*
                    ** Add Twitter Feed information
                    */
                    multithek.fraunhofer.getTwitterFeed(desc[0] + " " + data[slotid].title, function(data){
                        var statusMessages = data.statuses; //array with 15 elements
                        var tweetsDom = $('<ul class="tweets"></ul>');
                        if(statusMessages.hasOwnProperty("length") && statusMessages.length > 0){
                            //create list entries for each tweet
                            statusMessages.forEach(function(element){
                                var userimg = element.user.profile_image_url;
                                var content = element.text;
                                var hashtags = element.entities.hashtags;
                                //Tricky part: Change Hashtag entry with anchor Url to hashtag
                                if(element.entities.hashtags.length > 0){
                                    //multiple hashtags are possible
                                    hashtags.forEach(function(element){
                                        var rplc = "#"+element.text;
                                        var newStr = '<a href="https://www.twitter.com/hashtag/'+element.text+'" >#'+element.text+'</a>';
                                        content.replace(rplc,newStr);
                                    });
                                }
                                tweetsDom.append('<li class="tweet"><img src="'+userimg+'"><div class="content">'+content+'</div></li>');
                            });
                            $('.teaser.twitterFeed').append(tweetsDom);
                        }else{
                            //ToDo: What to do if there are no tweets given? -> Hide whole container ? Or Show a dummy image ? Or load multithek feed ?
                            //IF loading multithek feed -> embedded timelines from twitter can be used for this purpose -> https://dev.twitter.com/web/embedded-timelines
                        }
                    });
                    /*
                    ** Add Twitter Feed information
                    */
                    multithek.fraunhofer.getRecommendationForProgram(self.epgList, slotid,function(reco){
                        var recoHTMLList = $('<ul class="recommendedTVShows"></ul>');
                        var parent = $('.recommendations');
                        Object.keys(reco).forEach(function(program, index){//iterate over keys in object
                            var className = (index >= 3) ? "hidden":""; //for future paging of recommendations
                            var subtitle = (reco[program].subtitle !== null) ? reco[program].subtitle:"";
                            var info = reco[program].slotid.split(';'); // 0: Channel Name , 1: Timestamp
                            var recoHTMLString = '<li class="'+className+'"><div class="channelName">'+info[0]+'</div><div><strong>'+reco[program].title+'</strong> - '+subtitle+'</div></li>'
                            recoHTMLList.append(recoHTMLString);
                        });
                        parent.append(recoHTMLList);
                    });
                    //init tv show rating click listener for detail view
                    self.setTvShowRatingClickListener();
                });
            });
        }
    };

})(window.multithek = window.multithek || {}, $, moment);

