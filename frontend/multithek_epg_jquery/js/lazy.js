(function(multithek) {

    multithek.lazy = {

        $images: [],

        init: function() {
            this.$images = $('.image[data-src]');
            if(this.$images.length > 0){
             //  this.setLazyLoad();
            }else{
                this.$images = [];
            }
        },

        setLazyLoad: function() {
            addEventListener('scroll', this.checkImages, true);
            addEventListener('touchmove', this.checkImages, true);
            this.checkImages();
        },
        loadImage: function(elImg) {
            var img = new Image(),
                src = elImg.getAttribute('data-src');
                img.onload = function() {
                    elImg.style.backgroundImage= 'url(' + src + ')';
                };
            img.src = src;
            // todo: can be deleted. consider small dom or less dom operations.
            // elImg.removeAttribute('data-src');
        },
        elementInViewport: function(el) {
            var rect = el.getBoundingClientRect();
            return (
                rect.top    >= 0
                && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
            )
        },
        checkImages: function(){
            var img = multithek.lazy.$images;
            console.log('check images', img.length);
            for (var i = 0; i < img.length;++i) {
               // console.log(multithek.lazy.elementInViewport(img[i]));
                if (multithek.lazy.elementInViewport(img[i])) {
                    multithek.lazy.loadImage(img[i]);
                    img.splice(i, 1);
                }
            }

            if(img.length === 0){
                removeEventListener('scroll', this.checkImages);
                removeEventListener('touchmove', this.checkImages);
            }
        }

    };

    multithek.lazy.init();
})(window.multithek = window.multithek || {});