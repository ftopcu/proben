(function(multithek,$, moment) {

    multithek.stage = {

    }

//SETTING GLOBAL VARS
var animSpeed = 250;                        //ANIMATION SPEED
var pause = 5000;                           //PAUSE BETWEEN SLIDES
var interval;                               //STAGE TIMER
var player;                                 //DEFINE YOUTUBE PLAYER VARIABLE

var mainStage = $('#stage');
var stages = $('.stage', mainStage);
var stageChanger = $('#stagechanger');

function initStageContent(){
    var counter = 0;
    multithek.fraunhofer.getTipOfDay([],function(data){
        Object.keys(data).forEach(function(key, index){
            var slotid = key;
            multithek.fraunhofer.getProgramDetail(slotid,function(program){
                //define all necessary information
                var imageurl, title, description, start,length = "";
                var stageClass = (index == 0)? "stage active": "stage";
                var stageElement = $('<div class="'+stageClass+'"></div>');
                imageurl = (program[slotid].image_url !== null) ? program[slotid].image_url : "img/stage-testbild/testbild.jpg";
                title = program[slotid].title;
                description = program[slotid].description.slice(0,250) + "...";
                start = moment(program[slotid].begin).format("HH:mm") + " Uhr, " + moment(program[slotid].begin).format("dd") + ". " + moment(program[slotid].begin).format("DD.MM.");
                length = (moment(program[slotid].end)).diff( (moment(program[slotid].start)), "minutes") + " Min.";
                //append Image element
                var imgHtml = '<img width="980" height="396" alt="" src="'+imageurl+'" class="stageimg">';
                stageElement.append(imgHtml);
                var stageBox = '<div class="stagebox"><h2>'+title+'</h2><div class="date"><p>'+start+'</p></div><p>'+description+'</p><p class="tv">Video '+length+'</p></div>';
                stageElement.append(stageBox);
                if(index == 0){
                    mainStage.prepend(stageElement);
                }else{
                    mainStage.append(stageElement);
                }
                //create stageChanger element and append it
                var changerClass = (index == 0)? "active": " ";
                var changerElement = '<li class="'+changerClass+'"><div class="time"></div><a href="#">'+title+'</a></li>';
                if(index == 0){
                    stageChanger.prepend(changerElement);
                }else{
                    stageChanger.append(changerElement);
                }
                counter++;
                if(counter == Object.keys(data).length){
                    stages = $('.stage', mainStage);
                    mainStage.append('<a class="stage-nav prev" href="#">Prev</a>');
                    mainStage.append('<a class="stage-nav next" href="#">Next</a>');
                    initStageShow();
                }
            });
        });
    });
}

/*
* SEARCH THE ACTIVE NAVIGATION ELEMENT, ANIMATES THE TIMELINE AND RESET IT AT THE END
*/
function myTimer() {
    stageChanger.find('li.active .time').animate({
        width: '100%'
    },
    pause,
    'linear',
    function() {
        stageChanger.find('.time').css('width', '0');
    });
}

function initStageShow(){
        /*
    * STARTS THE AUTOMATIC SLIDING
    * EVERY 5 SECONDS THE ACTIVE STAGE ELEMENT GETS INVISIBLE AND THE NEXT ELEMENT GOES ACTIVE
    * SAME IN THE STAGE NAVIGATION
    */
    function startStage() {
        //STARTS THE TIMER THE FIRST TIME
        myTimer();
        interval = setInterval(function () {
            if (mainStage.find('.active').index()+1 == stages.length) {
                mainStage.find('.active').animate({
                    opacity: 0
                }).removeClass('active');
                mainStage.find('.stage:first-child').animate({
                    opacity: 1
                }).addClass('active');
                stageChanger.find('li.active').removeClass('active');
                stageChanger.find('li:first-child').addClass('active');
            }
            else {
                mainStage.find('.active').removeClass('active').animate({
                    opacity: 0
                }, animSpeed).next().addClass('active').animate({
                    opacity: 1
                }, animSpeed);

                idx = mainStage.find('.active').index();
                stageChanger.find('li.active').removeClass('active');
                stageChanger.find('li').eq(idx).addClass('active');
            }
            myTimer();
            pause = 5000;
        },
        pause);
    }

    /*
    * PAUSE THE AUTOMATIC STAGE-SLIDING
    */
    function pauseStage() {
        clearInterval(interval);
        stageChanger.find('li.active .time').stop(true, false);
    }


    //ADDING YOUTUBE SCRIPTS INTO DOM
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    //LOADING THE IFRAME INTO DIV WITH ID VIDEO
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('video', {
            height: '396',
            width: '704',
            videoId: 'fk24PuBUUkQ',
            events: {
                'onStateChange': onPlayerStateChange
            }
        });
        player2 = new YT.Player('playor', {
            height: '396',
            width: '704',
            videoId: 'e_xi8WscC-s',
            events: {
                'onStateChange': onPlayerStateChange
            }
        });
    }

    /*
    * PAUSE STAGE WHEN VIDEO IS PLAYING
    */
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING) {
            $('#stage, #stagechanger').off('mouseenter', pauseStage).off('mouseleave', startStage);
            pauseStage();
        }
        if (player.getPlayerState() == 2 || player2.getPlayerState() == 2) {
            $('#stage, #stagechanger').on('mouseenter', pauseStage).on('mouseleave', startStage);
        }
    }

    //bind mouseenter and mouseleave with the pause-function
    $('#stage, #stagechanger').on('mouseenter', function() {
        pauseStage();
        $('li.active .time').width(0);
    }).on('mouseleave', startStage);


    //changing stage and navigation with click in navigation
    stageChanger.find('a').on('click',function(e) {
        stageChanger.find('li.active .time').stop(true, true).css('width', 0);
        mainStage.find('.active').removeClass('active').animate({
            opacity: 0
        });
        stageChanger.parent().parent().find('li.active').removeClass('active');
        $(this).parent('li').addClass('active');

        idx = $(this).parent('li').index();

        stages.eq(idx).addClass('active').animate({
            opacity: 1
        });
        e.preventDefault();
    });


    //STAGE NAVIGATION FOR PREV BUTTON
    $('.stage-nav.prev').on('click', function(e) {

        stageChanger.find('li.active .time').stop(true, true).css('width', 0);
        if (mainStage.find('.active').index() == 0) {
            mainStage.find('.active').animate({
                opacity: 0
            }).removeClass('active');
            mainStage.find('.stage:last').animate({
                opacity: 1
            }).addClass('active');
            stageChanger.find('li.active').removeClass('active');
            stageChanger.find('li:last-child').addClass('active');
        }
        else {
            mainStage.find('.active').removeClass('active').animate({
                opacity: 0
            }, animSpeed).prev().addClass('active').animate({
                opacity: 1
            }, animSpeed);

            idx = mainStage.find('.active').index();
            stageChanger.find('li.active').removeClass('active');
            stageChanger.find('li').eq(idx).addClass('active');
        }
        e.preventDefault();
    });

    //STAGE NAVIGATION FOR NEXT BUTTON
    $('.stage-nav.next').on('click', function(e) {
        stageChanger.find('li.active .time').stop(true, true).css('width', 0);
        if (mainStage.find('.active').index()+1 == stages.length) {
            mainStage.find('.active').animate({
                opacity: 0
            }).removeClass('active');
            mainStage.find('.stage:first-child').animate({
                opacity: 1
            }).addClass('active');
            stageChanger.find('li.active').removeClass('active');
            stageChanger.find('li:first-child').addClass('active');
        }
        else {
            mainStage.find('.active').removeClass('active').animate({
                opacity: 0
            }, animSpeed).next().addClass('active').animate({
                opacity: 1
            }, animSpeed);

            idx = mainStage.find('.active').index();
            stageChanger.find('li.active').removeClass('active');
            stageChanger.find('li').eq(idx).addClass('active');
        }
        e.preventDefault();
    });
}

//START THE AUTOMATIC SLIDING
initStageContent();

})(window.multithek = window.multithek || {},$, moment);
