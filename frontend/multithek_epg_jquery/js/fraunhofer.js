(function(multithek, $) {
    //jQuery necessary dependancy
    /**
     * @class fraunhofer
     * used for communication with external servers
     * and things user token related
     */
     'use strict';


    multithek.fraunhofer = {

        onlyFavouriteChannels: false,
        onlyRecommendedChannels: false,
        activeChannelSource: 'all',
        activeChannelRegion: 'all',
        epgUrl: 'http://193.174.152.249', //basic web server url (fhg vm)
        proxyUrl: '/multithek/php/proxy.php?url=', //url to php proxy to request calls from the m.multithek.de backend
        epgProgramList: new Array(),
        recoUrl: "/HITV/CMS/reco.php?op=", //url to retrieve
        miniepgFile: "miniepg.json", //miniepg json file
        init: function() {
            var self = this;
            this._setChannelsForSettings(function(data){
                //initially we will retrieve all channels without any condition
                self.epgProgramList = data;
            });
        },

        login: function(formData, callback) {

        },

        register: function(formData, callback) {

        },

        contact: function(formData, callback) {

        },
        /*
        ** @function setChannelRecommendation - returns recommended channel list or all channels
        ** @input:
        **      state - indicates the state of recommendation false = all, true = recommended channels (?)
        **      callback - callback function which will receive epglist
        */
        setChannelRecommendation: function(state, callback) {
            this.onlyRecommendedChannels = state;
            if(this.onlyRecommendedChannels){
                //ToDo: Get recommended channel list
                this.getRecommendedList(callback);
            }
        },
        /*
        ** @function setChannelSource - returns channel list based on the channel source
        ** @input:
        **      value - indicates the source of channels (dvbt|livestream|all)
        **      callback - callback function which will receive (filtered) epglist
        */
        setChannelSource: function(value, callback) {
            this.activeChannelSource = value;
            if(this.activeChannelSource != 'all'){
                this.getChannelsForSource(callback);
            }else{
                this._setChannelsForSettings(callback);
            }
        },

        /*
        ** @function setChannelRegion - returns channel list based on the region
        ** @input:
        **      value - indicates the region for which the server returns a channellist (all|regionString(e.g. berlin, hamburg))
        **      callback - callback function which will receive (filtered) epglist
        */
        setChannelRegion: function(value, callback) {
            this.activeChannelRegion = value.trim();
            if(this.activeChannelRegion != 'all'){
                this.getChannelsForRegion(callback);
            }else{
                this._setChannelsForSettings(callback);
            }
        },

        /*
        ** @function showOnlyFavourites - returns only favourited list of channels
        ** @input:
        **      value - indicates the region for which the server returns a channellist (all|regionString(e.g. berlin, hamburg))
        **      callback - callback function which will receive (filtered) epglist
        */
        showOnlyFavourites: function(state, callback){
            this.onlyFavouriteChannels = state;
            if(this.onlyFavouriteChannels){
                this.getFavouriteList(callback);
            }
        },

        /*
        ** @function _setChannelsForSettings - returns channel list (helper method)
        ** @input:
        **      callback - callback function which will receive (filtered) epglist
        */
        _setChannelsForSettings: function(callback) {
            // called on success response from server
            if(typeof callback === 'function'){
                    this.setEpgList(callback);
            }
        },
        /*
        ** @function getChannelsForRegion -  ajax call which will request channel list based on stated region
        ** @input:
        **      callback - callback function which will receive (filtered) epglist
        */
        getChannelsForRegion: function(callback){
            var self = this;
                $.ajax({
                    url: this.epgUrl + '/cmsapi/epg/channellist/by/'+this.activeChannelRegion,
                    type: "GET"
                }).done(function(json){
                    // called on success response from server
                    if(typeof callback === 'function'){
                        var epgChannelList = json;
                        self.epgProgramList = json;
                        callback(epgChannelList);
                    }
                });
        },
         /*
        ** @function getChannelsForSource -  ajax call which will request channel list based on stated channel source
        ** @input:
        **      callback - callback function which will receive (filtered) epglist
        */
        getChannelsForSource: function (callback) {
            var self = this;
            $.ajax({
                url: this.epgUrl + '/cmsapi/epg/channellist/' + this.activeChannelSource,
                type: "GET"
            }).done(function(json){
                // called on success response from server
                if(typeof callback === 'function'){
                    var epgChannelList = json;
                    self.epgProgramList = json;
                    callback(epgChannelList);
                }
            });
        },
         /*
        ** @function getFavouriteList -  ajax call which will request favourite channels. If no favourite channels exist, it will receive an empty JSON object
        ** @input:
        **      callback - callback function which will receive (filtered) epglist
        */
        getFavouriteList: function(callback){
            var self = this;
            $.ajax({
                url: this.epgUrl + this.recoUrl + "getFavouriteChannels",
                type: "GET"
            }).done(function(json){
                var data = JSON.parse(json);
                //check for existing favourite list
                if(data.hasOwnProperty("length") && data.length > 0){
                    //We need to request all detail information for the channels
                    //the Reco-Engine is only returning a simple list with channel name and id
                    var detailData = [];
                    var deferreds = [];
                    //Create a list of ajax calls (promises)
                    data.forEach(function(element){
                       deferreds.push(self.getChannelInformation(element.id));
                    });
                    //process list of promises and return result via callback
                    //the result will be an object with different arguments
                    //$.when allows to process all promises and do some stuff after all calls are finished
                    //Details : http://api.jquery.com/jquery.when/
                    $.when.apply(null, deferreds).done(function(args){
                        //the chained ajax calls are finished
                        //$.when returns an array with multiple arguments
                        for(var i=0;i<arguments.length;i++){
                            if(arguments[i][1] == "success"){ //only on success
                                //$.when returns an Array with 3 elements [Array with json response,ajax status string, ajax object]
                                //Add detail Information of channel to list
                                detailData.push(arguments[i][0][0]);
                            }
                        }
                        // called on success response from server
                        if(typeof callback === 'function'){
                            self.epgProgramList = json;
                            callback(detailData);
                        }
                    });
                }else{
                    //The user has not favourized any channel
                    if(typeof callback === 'function'){
                        callback({});
                    }
                }
            });
        },
         /*
        ** @function setFavouriteState -  adds/removes tvChannel to favourite list 
        ** @input:
        **      tvChannel - channel ID of channel
        **      state - state of channel (true = is not in favourite channel , false = is in favourite channel)
        **      callback - callback function which will receive a basic success message from server
        */
        setFavouriteState: function(tvChannel, state, callback){
            console.log(typeof callback);
            if(!state){ //channel is currently not favored, but user has clicked on it therefore the state is "true"
                $.ajax({
                    url: this.epgUrl + this.recoUrl + "userFavsChannel&channelid="+tvChannel,
                    type: "GET"
                }).done(function(json){
                    // called on success response from server
                    if(typeof callback === 'function'){
                        callback();
                    }
                });
            }else{ //delete channel from list
                $.ajax({
                    url: this.epgUrl + this.recoUrl + "userDeletesChannelFav&channelid="+tvChannel,
                    type: "GET"
                }).done(function(json){
                    // called on success response from server
                    if(typeof callback === 'function'){
                        callback();
                    }
                });
            }
        },
         /*
        ** @function checkFavoriteState -  checks the state of an tvChannel (is this channel favored from the user?)
        ** @input:
        **      tvChannel - channel ID of channel
        **      callback - callback function which will receive a basic success message from server
        */
        checkFavoriteState: function(tvChannel, callback){
            $.ajax({
                url: this.epgUrl + this.recoUrl + "checkFavouriteChannel&channelid="+tvChannel,
                type: "GET"
            }).done(function(json){
                var resp = JSON.parse(json);
                // called on success response from server
                if(typeof callback === 'function'){
                    if(resp.status == "true"){ //channel is in favourite list
                        callback();
                    }
                }
            });
        },
         /*
        ** @function setRatingForShow -  sets rating for tvShow (program)
        ** @input:
        **      tvShow - slotID of tv program
        **      rating - new rating of tv program
        **      callback - callback function which will receive a basic success message from server
        */
        setRatingForShow: function(tvShow, rating, callback){
            var requestUrl = 'rateProgram&slotid='+tvShow+"&rating="+rating;
            $.ajax({
                url: this.epgUrl + this.recoUrl + requestUrl,
                type: 'GET'
            }).done(function(json){
                if(typeof callback === 'function'){
                    //success (empty response)
                    callback();
                }
            });
        },
         /*
        ** @function setEpgList -  helper method which requests epg channel list
        ** @input:
        **      callback - callback function which will receive full list of channels
        */
        setEpgList: function(callback){
            var epgChannelList = {};
            var self = this;
            $.ajax({
                url: this.epgUrl + '/cmsapi/epg/channellist',
                type: 'GET'
            }).done(function(json){
                epgChannelList = json;
                self.epgProgramList = json;
                callback(epgChannelList)
            });
        },
         /*
        ** @function setEpgPrograms -  helper method which requests basic epg program information from server (only slotid and title) for a given list of channels
        ** @input:
        **      epglist - channel list to retrieve basic epg programs information (doesn't check if it is full list or filtered list)
        **      callback - callback function which will receive full list of programs
        */
        setEpgPrograms: function(epglist, callback){
            //Lets create call query with epglist
            //The epglist will be transformed to a query string
            //m.multithek.de API 
            var query = 'resource.php?resource=2:esg:';
            epglist.forEach(function(element,index,array){
                query += element.title+',';
            });
            query += ':0';
            console.log(query);
            $.ajax({
                url: this.epgUrl + this.proxyUrl + encodeURIComponent(query),
                type: 'GET'
            }).success(function(json){
            	var data = JSON.parse(json);
            	callback(JSON.parse(json));
            });
        },
         /*
        ** @function getProgramDetail -  helper method which requests and returns detailed information for a given program (slotid)
        ** @input:
        **      slotid - channel list to retrieve basic epg programs information (doesn't check if it is full list or filtered list)
        **      callback - callback function which will receive detailed program info
        */
        getProgramDetail: function(slotid, callback){
        	//m.multithek.de API call
        	var requestUrl = 'reco.php?op=getProgram&slotid='+slotid;
        	$.ajax({
        		url: this.epgUrl + this.proxyUrl + encodeURIComponent(requestUrl),
        		type: 'GET'
        	}).success(function(json){
        		callback(JSON.parse(json));
        	});
        },
         /*
        ** @function getRegions -  helper method which requests and returns list of regions in DB
        ** @input:
        **      callback - callback function which will receive full list of channels
        */
        getRegions: function(callback){
            var regions;
            $.ajax({
                url: this.epgUrl + '/cmsapi/epg/regionlist',
                type: 'GET'
            }).done(function(json){
                regions = json
                callback(regions);
            });
        },
         /*
        ** @function getMiniEpgChannels -  helper method calls miniepg.json file to receive list of mini epg channels
        ** @input:
        **      callback - callback function which will receive list of channels titles
        */
        getMiniEpgChannels: function(callback){
            $.ajax({
                url: this.miniepgFile,
                type: 'GET',
            }).done(function(json){
                if(typeof callback === 'function'){
                    callback(json);
                }
            });
        },
         /*
        ** @function getChannelInformation -  helper method for chaining ajax calls, returns ajax promise
        ** @input:
        **      channelid - id of channel to request detail information
        */
        getChannelInformation: function(channelid){
            return $.ajax({
                url: this.epgUrl + '/cmsapi/epg/channel/'+channelid,
                type: 'GET'
            });
        },
         /*
        ** @function getPrettyTime -  helper method which returns a nice timestamp representation
        ** @input:
        **      timestamp - timestamp (in milliseconds)
        */
        getPrettyTime: function(timestamp){
        	var hours = (new Date(timestamp*1000)).getHours();
        	if(hours <= 9){ //Adding a leading 0 for Hour times
        		hours = '0'+hours;
        	}
        	var minutes = (new Date(timestamp*1000)).getMinutes();
        	if(minutes <= 9){
        		minutes = '0'+minutes;
        	}
        	return (hours + ':' + minutes + ' Uhr');
        },
        /*
        ** @function getStreamUrls -  helper method which returns the stream_url (urls) for a given channelname
        ** @input:
        **      epglist - the epglist to search for the channel
        **      channelName - name of the channel
        */
        getStreamUrls: function(epglist, channelName, callback){
            var channel = {};
            epglist.forEach(function(element, index){
                if(element.title === channelName){
                    channel = element;
                }
            });
            this.getChannelInformation(channel.id).done(function(json){
                var data = json;
                var urls = data[0].stream_url;
                if(typeof callback === 'function'){
                    callback(urls);
                }
            });

        },
        /**
        * Recommendation Calls
        **/
        /*
        ** @function getRecommendationForProgram -  returns a list of recommended tv shows for a given slotid
        ** @input:
        **      epglist - epglist (necessary to build request query)
        **      slotid - slotid of tv show for which to receive recommendations
        **      callback - callback which will receive a list of recommended tv-shows (slotids)
        */
        getRecommendationForProgram: function(epglist, slotid, callback){
            var channelsString = "";
            epglist.forEach(function(element){
                channelsString += element.title+",";
            });

            var requestUrl = "reco.php?op=getRelatedPrograms&slotid="+slotid+"&channels="+channelsString;
            $.ajax({
                url: this.epgUrl + this.proxyUrl + encodeURIComponent(requestUrl),
                type: 'GET'
            }).done(function(json){
                var data = JSON.parse(json);
                if(typeof callback === 'function'){
                    callback(data);
                }
            });
        },
        /*
        ** @function getTipOfDay -  returns a list of 5 tv shows which are recommended for this day (content-bühne)
        ** @input:
        **      epglist - epglist (necessary to build request query)
        **      callback - callback which will receive a list of recommended tv-shows (slotids)
        */
        getTipOfDay: function(epglist, callback){
            //create channel string
            var channelsString = "";
            epglist.forEach(function(element){
                channelsString += element.title+",";
            });
            var day = moment().format("DD.MM.YYYY");
            var requestUrl = 'getDayTip&tvday='+day+"&channels="+channelsString;
            $.ajax({
                url: this.epgUrl + this.recoUrl + requestUrl,
                type: 'GET'
            }).done(function(json){
                var data = JSON.parse(json);
                if(typeof callback === 'function'){
                    callback(data);
                }
            });
        },
        /*
        ** @function getTwitterFeed -  function to receive a list of tweets (15) for a given search query
        ** @input:
        **      search - string with key words
        **      callback - callback which will receive a list tweets
        */
        getTwitterFeed: function(search, callback){
            var requestUrl = "/twitterfeed/index.php?search="+search;
            $.ajax({
                url: this.epgUrl + requestUrl,
                type: 'GET'
            }).done(function(json){
                var data = JSON.parse(json);
                if(typeof callback === 'function'){
                    callback(data);
                }
            });
        },
        /*
        ** @function initVideo -  helper method for initializing video player
        ** @input:
        **      parent - parent DOM (video ) element
        **      urls - Array which has one or more livestream urls
        */
        initVideo: function(parent, urls){
            var v = document.createElement("video");
            v.controls="true";
            v.preload = "none";
            v["data-setup"] = '{"techOrder": ["html5","flash"]}';
            for(var i in urls){
                var src = document.createElement("source"); 
                src.src = urls[i].src;
                src.type = urls[i].type;
                v.appendChild(src);
            }   
            parent.appendChild(v);
            v.className = v.className + " video-js vjs-default-skin vjs-big-play-centered";
            videojs(v, {}, function(){});
        }
    };

    multithek.fraunhofer.init();
})(window.multithek = window.multithek || {}, $);