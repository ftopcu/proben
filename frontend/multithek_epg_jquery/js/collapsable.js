(function(multithek) {

    multithek.collapsable = {

        init: function() {
            if($('.collapsable').length > 0){
                this.setClickListener();
            }
        },

        setClickListener: function() {
            $('.collapsable').find('h4, h5').on('click', function(e){
                $(this).parent().toggleClass('open');
            });
        }

    };

    multithek.collapsable.init();
})(window.multithek = window.multithek || {});

