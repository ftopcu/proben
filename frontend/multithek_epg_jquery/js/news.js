(function(multithek) {

    multithek.news = {

        init: function() {
            if($('.newscolumn').length > 0){
                this.setClickListener();
            }
        },

        setClickListener: function() {
            $('.moreLess').on('click', function(e){
                e.preventDefault();
                $(this).parent().toggleClass('open');
            });
        }

    };

    multithek.news.init();
})(window.multithek = window.multithek || {});

