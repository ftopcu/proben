(function(multithek) {

    multithek.main = {
        init: function() {
            multithek.epg.init();
        }
    };

    multithek.main.init();
})(window.multithek = window.multithek || {});