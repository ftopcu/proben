(function(multithek, moment) {
    "use strict";
    moment.locale('de');
    multithek.miniepg = {

        // will be set on page setup; used to trigger some elements to be fixed after passing this scroll position
        $progressBars: [],

        init: function() {
            if($('.mini-epg').length > 0){
                this.setMiniEPGContent();
                this.setPlayClickListener();
                this.setProgressEvents();
            }
        },

        // open video player on click
        setPlayClickListener: function() {
            $('.mini-epg').find('.playButton').on('click', function(e){
                e.preventDefault();

                // todo: call layer player
                var streamId = this.getAttribute('data-stream');
                console.log('implement me, please. opening layer with streamId = ' + streamId);

                return false;
            });
        },

        //
        setProgressEvents: function() {
            this.$progressBars = $('.mini-epg').find('span');
            var onTick = function(){
                var progress = 0,
                    bar = null,
                    currentTime = 0,
                    totalTime = 0;

                if(multithek.miniepg.$progressBars.length === 0){
                    clearInterval(tickInterval);
                }
                for(var i = multithek.miniepg.$progressBars.length - 1; i >= 0; --i){
                    bar = multithek.miniepg.$progressBars[i];
                    currentTime = parseInt(bar.getAttribute('data-currentTime'), 10);
                    totalTime = parseInt(bar.getAttribute('data-totalTime'), 10);
                    progress = currentTime / totalTime * 100;
                    bar.style.width = ( progress << 0 ) +'%';
                    if(currentTime < totalTime) {
                        bar.setAttribute('data-currentTime', currentTime + 1);
                    }else{
                        bar.style.borderRightWidth = 0;
                        multithek.miniepg.$progressBars.splice(i,1);
                        $(bar).closest('.channel').find('.playButton').off('click').addClass('finished');
                    }
                }
            },
                tickInterval = setInterval(onTick, 60000);
            onTick();

        },
        /*
        ** @function setMiniEPGContent -  initializes mini-epg
        */
        setMiniEPGContent: function(){
            var self = this;
            var parentElement = $('.teaser.mini-epg');
            //Add Header description Text
            parentElement.append('<h3 class="icon-title icon-mini-epg">Jetzt live im TV</h3>');
            //retrieve channel list for Mini-EPG
            multithek.fraunhofer.getMiniEpgChannels(function(epglistObject){
                //init parent channel HTML element
                Object.keys(epglistObject).forEach(function(element, index){
                    var times = element.split("-");
                    var currentTime = moment().format("HH:mm"); //current time in HH:mm Format
                    //Check: At which time slot are we?
                    //check if currentTime is for the first time slot or for the second one (in miniepg.json)
                    if( currentTime >= times[0] && currentTime <= times[1] ){ 
                        //based on that, retrieve tv shows for given channels
                        multithek.fraunhofer.setEpgPrograms(epglistObject[element].channels, function(data){
                            var programs = data.grid.channels;
                            //iterate over slotids
                            //searching for the current running tv show
                            Object.keys(programs).forEach(function(channel,index){
                                var channelDom = $('<div class="channel"></div>');
                                var listOfPrograms = programs[channel];
                                //It is possible that we don't have any epg data and the server returns only one object
                                // in this case use just this one element
                                if(listOfPrograms.length == 1){
                                    var element = listOfPrograms[0];
                                    //if there is no ip-stream epg than lets add some basic content
                                    var progress = '<div class="progress"></div>';
                                    channelDom.append(progress);
                                    var info = element.id.split(';'); // 0: Channel Name , 1: Timestamp
                                    var subtitle = "";
                                    var anchors = '<a href="/multithek/epg.html" class="name">'+info[0]+'</a><a href="#/multithek/tvShow-Detail.html#'+element.id+'" class="showTitle"><h5 title="'+element.title+' -">'+element.title+' -</h5><h6 title="'+subtitle+'">'+subtitle+'</h6></a>';
                                    channelDom.append(anchors);
                                }else{
                                    //Iterate over the list of tv shows
                                    listOfPrograms.forEach(function(element){
                                        //check: just to be sure that we have a start and end timestamp
                                        if(typeof element.begin != "undefined" && typeof element.end != "undefined"){
                                            var begin = moment(element.begin*1000);
                                            var end = moment(element.end*1000);
                                            var now = moment();
                                            //check: is the current time (now) greater than the beginning of the tv show and smaller than the end time
                                            if(now.diff(begin, "minutes") >= 0 && end.diff(now,"minutes") > 0){
                                                //We found the current element
                                                var totalTime = end.diff(begin, "minutes");
                                                var currentTime=now.diff(begin, "minutes");
                                                //create progress bar
                                                var progress = '<div class="progress"><span data-totalTime="'+totalTime+'" data-currentTime="'+currentTime+'"></div>';
                                                channelDom.append(progress);
                                                //add some info
                                                var info = element.id.split(';'); // 0: Channel Name , 1: Timestamp
                                                //var subtitle = (element.subtitle !== null) ? element.subtitle:"";
                                                var subtitle = "";
                                                var anchors = '<a href="/multithek/epg.html" class="name">'+info[0]+'</a><a href="#/multithek/tvShow-Detail.html#'+element.id+'" class="showTitle"><h5 title="'+element.title+' -">'+element.title+' -</h5><h6 title="'+subtitle+'">'+subtitle+'</h6></a>';
                                                channelDom.append(anchors);
                                                return false;
                                            }
                                        }
                                    });
                                }
                                parentElement.append(channelDom);
                            });
                            //add Button
                            parentElement.append('<a class="button" href="">» Alle Programme anzeigen</a>');
                        });
                    }
                });
                //retrieve programs for channels
            });
        }
    };

    multithek.miniepg.init();
})(window.multithek = window.multithek || {}, moment);

