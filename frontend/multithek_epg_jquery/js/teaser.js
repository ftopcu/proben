(function(multithek,$, moment) {
    "use strict";
    moment.locale('de');
    multithek.teaser = {
    	init: function(){
    		this.setTeaserContent();
    	},
        /*
        ** @function setTeaserContent -  initializes teaser
        */
    	setTeaserContent: function(){
    		var currentTime = moment();
            var currentTimeString = currentTime.format("HH:mm") + " Uhr, " + currentTime.format("dd") + ". " + currentTime.format("DD.MM.");
            //get parent teaser element
            var teaserParent = $('.teaser-livestream');
            //initialize basic teaser layout (header with current Time and containers)
            teaserParent.append('<div class="time"><span>'+currentTimeString+'</span></div><div class="teaser-livestream-container"><div class="teaser-livestream-content"></div></div>');
            //get content container for teaser
            var teaser = $('.teaser-livestream-content');
            //retrieve all livestreams
            multithek.fraunhofer.setChannelSource("livestream", function(channellist){
                //We are selecting a random livestream channel
            	var num = Math.floor(Math.random()*channellist.length);
            	var channels = [];
            	channels.push(channellist[num]);
                //retrieve tv show informations for the randomly selected livestream channel
            	multithek.fraunhofer.setEpgPrograms(channels,function(data){
            		var programs = data.grid.channels;
                        //iterate over slotids
                        //searching for the current running tv show
                		Object.keys(programs).forEach(function(channel,index){
            			var listOfPrograms = programs[channel];
                            //It is possible that we don't have any epg data and the server returns only one object
                            // in this case use just this one element
                            if(listOfPrograms.length == 1){
                                var element = listOfPrograms[0];
                                var info = element.id.split(';'); // 0: Channel Name , 1: Timestamp
                                teaser.append('<p class="broadcaster">'+info[0]+'</p>');
                                teaser.append('<h2 class="teaser-livestream-headline">'+element.title+'</h2>');
                                teaser.append('<a class="teaser-play-button" href=""></a><a class="teaser-info-button" title="" href=""></a>');
                            }else{
                                //Iterate over the list of tv shows
                            	listOfPrograms.forEach(function(element){
                            		var begin = moment(element.begin*1000);
                                    var end = moment(element.end*1000);
                                    var now = moment();
                                    //check: is the current time (now) greater than the beginning of the tv show and smaller than the end time
                                    if(now.diff(begin, "minutes") >= 0 && end.diff(now,"minutes") > 0){
                                        //We found the current element
                                        var totalTime = end.diff(begin, "minutes");
                                        var currentTime=now.diff(begin, "minutes");
                                        var info = element.id.split(';'); // 0: Channel Name , 1: Timestamp
                                        //create content DOM
		                                teaser.append('<p class="broadcaster">'+info[0]+'</p>');
		                                teaser.append('<h2 class="teaser-livestream-headline">'+element.title+'</h2>');
		                                teaser.append('<a class="teaser-play-button" href=""></a><a class="teaser-info-button" title="" href=""></a>');
                                        return false;
                                    }
                            	});
                            }
            		});
            	});
            });
    	}
    };
    multithek.teaser.init();
})(window.multithek = window.multithek || {}, $, moment);