/**
 * Created by ferittopcu on 13.11.14.
 */
(function(){

    var senderCMS = angular.module('senderCMS', ['senderCMSControllers', 'senderCMSServices', 'ui.sortable', 'ngResource', 'ngRoute', 'ui.navigationbar', 'ui.bootstrap', 'ui.addstreamurl']);

    senderCMS.config(['$routeProvider','$locationProvider',
        function($routeProvider, $locationProvider) {
//================================================
            // Define all the routes
            //================================================
            $routeProvider
                //Start page, where the user gets a list of registered SO and can add new ones
/*                .when('/', {
                    templateUrl: 'templates/login.html',
                    controller: 'loginCtrl'
                })*/
                .when('/', {
                    templateUrl: 'templates/start.html',
                    controller: 'startCtrl'
                })
                .when('/detail/:id',{
                    templateUrl: 'templates/detail.html',
                    controller: 'detailCtrl'
                })
                .when('/new', {
                    templateUrl: 'templates/new.html',
                    controller: 'newCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }]);

})();