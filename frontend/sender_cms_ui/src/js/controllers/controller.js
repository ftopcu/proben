/**
 * Created by ferittopcu on 13.11.14.
 * Controller for senderCMS App
 */
(function () {

        var senderCMSApp = angular.module('senderCMSControllers', []);
        //Helper Class which adds a remove function for the basic array object
        Array.prototype.remove = function(from, to) {
          var rest = this.slice((to || from) + 1 || this.length);
          this.length = from < 0 ? this.length + from : from;
          return this.push.apply(this, rest);
        };
    /**
     * Our login app controller
     */
    senderCMSApp.controller('loginCtrl', ['$scope', '$rootScope','$location',
        function($scope, $rootScope, $location){
            $scope.username = "";
            $scope.password = "";
            $scope.loggedIn = false;
            $scope.loginError = false;
            $scope.errorMessage = "Sie haben falsche Benutzerdaten angegeben. Bitte kontaktieren sie den Administrator.!";
            $scope.login = function(){
                if($scope.username == "superuser" && $scope.password == "fame@fame"){
                    $location.url('/start');
                }else{
                    $scope.loginError = true;
                }
            };

            /**
             * Better function for dismissing alert box
             * standard bootstrap function is dismissing whole DOM element
             */
            $scope.dismissWarning = function(){
                $scope.loginError = false;
            };

        }]);

    /**
     *
     */
    senderCMSApp.controller('startCtrl', ['$scope', '$rootScope', '$location','EPG',
        function($scope, $rootScope, $location, epg){
            $scope.loggedIn = true;
            $scope.epglist = {};
            $scope.errorMessage = "";
            $scope.clickToChangeText = "Zum Bearbeiten der Senderliste hier klicken";
            $scope.smallText = "Zum Bearbeiten ";
            $scope.hasError = false;
            $scope.isSortable = false;
            $scope.sortableOptions = {
                update: function(e, ui) {
                    if (!$scope.isSortable) {
                        ui.item.sortable.cancel();
                    }
                }
            };

            //Get EPG list
            epg.getEPGList().then(function(data){
                $scope.epglist = data;
            }, function(error){
                $scope.errorMessage = error.message;
            });

            /**
             * Better function for dismissing alert box
             * standard bootstrap function is dismissing whole DOM element
             */
            $scope.dismissWarning = function(){
                $scope.loginError = false;
            };

            /**
             * Helper functions which allows the manipulation of the channel list
             */
            $scope.allowSortable = function(){
                if($scope.isSortable){
                    $scope.isSortable = false;
                }else{
                    $scope.isSortable = true;
                }
            };
            /**
             * Redirect Detail Page
             */
            $scope.showDetails = function(channel){
              if(typeof channel.id != "undefined"){
                  $location.url("/detail/"+channel.id);
              }
            };

            /**
             * Updates EPG LIST
             */
            $scope.updateEpg = function(epglist){
                epglist.forEach(function(element, index, array){
                    if(element.order_nr != (index+1).toString()){
                        element.order_nr = (index+1).toString();
                    }
                });
                epg.updateEpg(epglist).then(function(data){
                   console.log(data);
                    $scope.isSortable = false;
                });
            };
        }]);

        senderCMSApp.controller('detailCtrl', ['$scope', '$routeParams', 'EPG','$location','$modal',
            function($scope, $routeParams,epg, $location, $modal){
                $scope.loggedIn = true;
                $scope.channel = {};
                $scope.transformRegion = "";
                $scope.types = globals.channelTypes;
                $scope.metaTypes = globals.metaTypes;
                $scope.recoGroups = globals.recoGroups;
                $scope.selectedType = {};
                $scope.streamTypes = globals.streamTypes;
                epg.getChannel($routeParams.id).then(function(channel){
                   $scope.channel = channel[0];
                   //To pre-select Sendertyp DropDown
                  var i;
                   for(i=0;i<$scope.types.length;i++){
                    if($scope.channel.channel_type === $scope.types[i].name){
                      $scope.channel.type = $scope.types[i];
                    }
                   }
                   //To pre-select Stream Typ of stream urls
                   for(i=0;i<$scope.streamTypes.length;i++){
                    for(var j =0;j<$scope.channel.stream_urls.length;j++){
                      if($scope.channel.stream_urls[j].type === $scope.streamTypes[i].type){
                         $scope.channel.stream_urls[j].type = $scope.streamTypes[i];
                      }
                    }
                   }
                   //to pre-select metadata type
                   for(i=0;i<$scope.metaTypes.length;i++){
                    if($scope.channel.sourcetype === $scope.metaTypes[i].name){
                      $scope.channel.sourcetype = $scope.metaTypes[i];
                    }
                   }
                   //Update Sender Gruppe
                   if($scope.channel.sourcetype.name == "rtv"){
                    for( i=0;i<$scope.recoGroups.length;i++){
                      if($scope.channel.reco_group === $scope.recoGroups[i].name){
                        $scope.channel.reco_group = $scope.recoGroups[i];
                      }
                    }
                   }

                }, function(error){
                    //ToDo
                });

                $scope.updateChannel = function(){
                    $scope.channel.sourcetype = $scope.channel.sourcetype.name; //override JSON Structure with basic String
                    if($scope.channel.sourcetype == 'rtv'){
                      $scope.channel.reco_group = $scope.channel.reco_group.name;
                    }else{
                      $scope.channel.reco_group = null;
                    }                   
                    $scope.channel.channel_type = $scope.channel.type.name;
                    delete $scope.channel.type;
                    if($scope.channel.channel_type == "IP-Stream"){
                      $scope.channel.stream_urls.forEach(function(element){
                        element.type = element.type.type;
                      });
                    }
                    epg.updateChannel($scope.channel).then(function(success){
                       $location.url("/start");
                    }, function(error){
                        //ToDo
                    });
                };

                $scope.removeStreamUrl = function(index){
                  //Removing element from array
                  console.log("Removing url at ", index);
                  $scope.channel.stream_urls.remove(index);
                };

                $scope.typeChange = function(){
                  console.log($scope.channel.type);
                };
                //Definition for ui.bootstrap modal directive!
                $scope.openModal = function (channel, $event) {

                  var modalInstance = $modal.open({
                    templateUrl: 'myModalContent.html',
                    controller: 'ModalInstanceCtrl'
                  });

                  modalInstance.result.then(function () {
                    epg.deleteChannel(channel).then(function(done){
                        $location.url("/start");
                    });  
                  }, function () {
                  });
                  // On recent browsers, only $event.stopPropagation() is needed
                  if ($event.stopPropagation) $event.stopPropagation();
                  if ($event.preventDefault) $event.preventDefault();
                  $event.cancelBubble = true;
                  $event.returnValue = false;
                };

            }

        ]);

        senderCMSApp.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 
          function($scope, $modalInstance){
              $scope.ok = function () {
                $modalInstance.close();
              };

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }]);

        senderCMSApp.controller('newCtrl', ['$scope', 'EPG','$location',
            function($scope, epg, $location){
                $scope.loggedIn = true;
                $scope.channel = new epg.Channel();
                $scope.transformRegion = "";
                $scope.types = globals.channelTypes;
                $scope.metaTypes = globals.metaTypes;
                $scope.recoGroups = globals.recoGroups;
                $scope.lastOrderNr = "";
                $scope.streamTypes = globals.streamTypes;
                //Get EPG list
                epg.getEPGList().then(function(data){
                    $scope.channel.order_nr = data.length + 1;
                }, function(error){
                    $scope.errorMessage = error.message;
                });
                //initialize some values
                $scope.channel.default = "0"; //default value: 0
                $scope.channel.tag = null;
                $scope.channel.tag2 = null;
                $scope.channel.stream_urls = [];
                $scope.create = function(){
                    $scope.channel.sourcetype = $scope.channel.sourcetype.name; //override JSON Structure with basic String
                    if($scope.channel.sourcetype == 'rtv'){
                      $scope.channel.reco_group = $scope.channel.reco_group.name;
                    }else{
                      $scope.channel.reco_group = null;
                      $scope.channel.tag = "NULL";
                    }
                    $scope.channel.channel_type = $scope.channel.type.name;
                    if($scope.channel.channel_type == "IP-Stream"){
                      $scope.channel.stream_urls.forEach(function(element){
                        element.type = element.type.type;
                      });
                    }
                    delete $scope.channel.type;
                    epg.createChannel($scope.channel).then(function(success){
                            $location.url("/start");
                        },
                    function(error){
                        //ToDo
                    });
                };
            }
        ]);

})();