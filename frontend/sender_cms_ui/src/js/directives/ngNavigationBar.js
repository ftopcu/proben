/**
 * Created by ferittopcu on 13.11.14.
 * Directive for navigation bar
 */
(function () {

    var demoAppDirectives = angular.module('ui.navigationbar', []);

    demoAppDirectives.directive('ngNavigationBar', [
        function(){
            return {
                restrict: 'EA',
                scope:{
                  "ngState": "="
                },
                template: '<div class="navbar navbar-inverse"> <div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </button><a class="navbar-brand" href="#">Multithek Sender-CMS</a> </div><div class="navbar-collapse collapse navbar-responsive-collapse"><ul class="nav navbar-nav navbar-right" ng-if="ngState"><li><a href="#/">Senderübersicht</a></li><li><a href="#/new"><span class="glyphicon glyphicon-plus"></span> Neuen Kanal erstellen</a></li><li><a href="#/logout">Logout</a></li> </ul> </div> </div>',
            };
        }]);

})();