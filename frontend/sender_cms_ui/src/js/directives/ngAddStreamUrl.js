(function () {
  "use strict";


  /* Directive */

  var soManagerAppDirectives = angular.module('ui.addstreamurl', []);
  /*
   * Directive for adding an IP-Stream entry for a new channel
   * template is a simple button and onclick new input options are displayed
   */
  soManagerAppDirectives.directive('ngAddStreamUrl', ['$compile',
    function ($compile) {
      return {
        restrict: 'EA',
        template: '<a class="btn btn-primary" type="submit"><span ng-if="isSortable" class="glyphicon glyphicon-align-justify"></span><span>IP-Stream hinzufügen </span></a>',
        link: function ($scope, element, attrs) {
          element.bind("click", function () {//Click Handler for Adding new ip stream information
            //new position of the ip stream entry
            if ($scope.channel.stream_urls === null) {
              $scope.channel.stream_urls = [];
            }
            //create empty entry
            $scope.channel.stream_urls.push({"type": "", "stream_url": ""});
            var lastElement = $scope.channel.stream_urls.length - 1;
            //Adding new DOM Elements after user is clicking to the button
            angular.element(this).parent().parent().after($compile('<br> <div class="form-group"><label for="soDesc" class="col-lg-2 control-label">Stream Typ</label><div class="col-lg-10"><select class="form-control" id="soDesc" ng-model="channel.stream_urls[' + lastElement + '].type" ng-options="t.name for t in streamTypes"><option value="">-- Bitte einen Sendertypen auswählen --</option></select></div></div><div class="form-group"><label for="soInput" class="col-sm-2 control-label">Stream Url</label><div class="col-sm-8"><input type="text" class="form-control" id="soInput" placeholder="Stream Url" ng-model="channel.stream_urls[' + lastElement + '].stream_url"></div></div><hr class="form-group-divider">')($scope));
          });
        }
      };
    }]);

  /*
   * Directive for adding an IP-Stream entry within Edit page
   * * creates an empty JSON representation
   */
  soManagerAppDirectives.directive('ngAddStreamUrlLight', ['$compile',
    function ($compile) {
      return {
        restrict: 'EA',
        template: '<a class="btn btn-primary" type="submit"><span ng-if="isSortable" class="glyphicon glyphicon-align-justify"></span><span>IP-Stream hinzufügen </span></a>',
        link: function ($scope, element, attrs) {
          element.bind("click", function () {
            //Click Handler for Adding new ip stream information
            //new position of the ip stream entry
            if ($scope.channel.stream_urls === null) {
              $scope.channel.stream_urls = [];
            }
            $scope.$apply(function () {
              //create empty entry
              $scope.channel.stream_urls.push({"fk_channel_id": $scope.channel.id, "type": "", "stream_url": ""});
            });
          });
        }
      };
    }]);
}());