/**
 * Created by ferittopcu on 13.11.14.
 * Main file for the definition of services
 * Project: senderCMS
 */
(function() {
    'use strict';

    var senderCMSServices = angular.module('senderCMSServices', ['ngResource']);

    senderCMSServices.factory('EPG', ['$resource','$http','$q','$rootScope',
        function($resource, $http, $q, $rootScope){

            var channel = function(){
                var channel = {
                    "title": "",
                    "type":"",
                    "stream_urls": [],
                    "region":""
                };
                return channel;
            };

            function parseJSON(data, headers){
                if(headers('Content-Type') == "text/html"){
                    var newdata = data.slice(3);
                    return JSON.parse(newdata);
                }
            }

            function getChannellist(){
                var deferred = $q.defer();
                $http({method: 'GET', url: globals.getEpgUrl()}).
                    success(function(data,status,headers, config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                       deferred.reject({error: status, message: "Konnte die EPG Liste nicht abrufen."});
                    });
                return deferred.promise;
            }

            function updateChannellist(list){
                var deferred = $q.defer();
                $http({method: "PUT", data: list, url: globals.getEpgUrl()}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function getChannelDetails(id){
                var deferred = $q.defer();
                $http({method: "GET", url: globals.getChannelUrl(id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function updateChannel(channel){
                var deferred = $q.defer();
                var channelCopy = {};
                angular.copy(channel, channelCopy);
                delete channelCopy.id; //Id is not necessary
                delete channelCopy.type; // type is only an angularjs internal helper -> not necessary
                $http({method: "PUT", data: channelCopy, url: globals.getChannelUrl(channel.id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function createChannel(channel){
                var deferred = $q.defer();
                $http({method: "POST", data: channel, url: globals.getChannelUrl()}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function deleteChannel(channel){
                var deferred = $q.defer();
                 $http({method: "DELETE", url: globals.getChannelUrl(channel.id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }


            return{
                getEPGList: getChannellist,
                getChannel: getChannelDetails,
                updateChannel: updateChannel,
                createChannel: createChannel,
                Channel: channel,
                updateEpg: updateChannellist,
                deleteChannel: deleteChannel
            };
        }
    ]);
})();