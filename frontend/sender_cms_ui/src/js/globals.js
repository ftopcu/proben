/**
 * Created by ferittopcu on 18.11.14.
 * The globals File stores all configuration parameters, e.g. HOST, PORT and API call Endpoints
 */

var globals =( function(){
    var HOST = "http://193.174.152.249";
    var PORT = "80";
    var epgMainUrl = "/cmsapi/epg/channellist";
    var channelDetailUrl = "/cmsapi/epg/channel";
    var groupNames = [
        {"name": "Hauptsender"},
        {"name": "Dritte"},
        {"name": "Regionale"},
        {"name": "Ausland"},
        {"name": "Sonstige Deutsche"}
    ];
    var metaTypes = [
        {"name": "rtv"},
        {"name": "static"}
    ];

    var channelTypes =[
        {"name": "IP-Stream"},
        {"name": "DVB-T"}
    ];

    var streamTypes = [
        {"name": "DASH (.mpd)", "type": "application/dash+xml"},
        {"name": "HLS (.m3u8)", "type": "application/vnd.apple.mpegurl"},
        {"name": "HDS (.f4m)", "type": "application/f4m"}
    ];

    function returnChannelUrl(id){
        if(typeof id == "undefined"){
            return HOST + channelDetailUrl;
        }else{
            return HOST + channelDetailUrl+"/"+id;
        }
    }

    return{
        host : HOST,
        port: PORT,
        recoGroups: groupNames,
        channelTypes: channelTypes,
        metaTypes: metaTypes,
        streamTypes: streamTypes,
        getEpgUrl: function(){ return HOST + epgMainUrl;},
        getChannelUrl: returnChannelUrl
    };
})();