/**
 * Created by ferittopcu on 13.11.14.
 * Controller for senderCMS App
 */
(function () {

        var senderCMSApp = angular.module('senderCMSControllers', []);
        //Helper Class which adds a remove function for the basic array object
        Array.prototype.remove = function(from, to) {
          var rest = this.slice((to || from) + 1 || this.length);
          this.length = from < 0 ? this.length + from : from;
          return this.push.apply(this, rest);
        };
    /**
     * Our login app controller
     */
    senderCMSApp.controller('loginCtrl', ['$scope', '$rootScope','$location',
        function($scope, $rootScope, $location){
            $scope.username = "";
            $scope.password = "";
            $scope.loggedIn = false;
            $scope.loginError = false;
            $scope.errorMessage = "Sie haben falsche Benutzerdaten angegeben. Bitte kontaktieren sie den Administrator.!";
            $scope.login = function(){
                if($scope.username == "superuser" && $scope.password == "fame@fame"){
                    $location.url('/start');
                }else{
                    $scope.loginError = true;
                }
            };

            /**
             * Better function for dismissing alert box
             * standard bootstrap function is dismissing whole DOM element
             */
            $scope.dismissWarning = function(){
                $scope.loginError = false;
            };

        }]);

    /**
     *
     */
    senderCMSApp.controller('startCtrl', ['$scope', '$rootScope', '$location','EPG',
        function($scope, $rootScope, $location, epg){
            $scope.loggedIn = true;
            $scope.epglist = {};
            $scope.errorMessage = "";
            $scope.clickToChangeText = "Zum Bearbeiten der Senderliste hier klicken";
            $scope.smallText = "Zum Bearbeiten ";
            $scope.hasError = false;
            $scope.isSortable = false;
            $scope.sortableOptions = {
                update: function(e, ui) {
                    if (!$scope.isSortable) {
                        ui.item.sortable.cancel();
                    }
                }
            };

            //Get EPG list
            epg.getEPGList().then(function(data){
                $scope.epglist = data;
            }, function(error){
                $scope.errorMessage = error.message;
            });

            /**
             * Better function for dismissing alert box
             * standard bootstrap function is dismissing whole DOM element
             */
            $scope.dismissWarning = function(){
                $scope.loginError = false;
            };

            /**
             * Helper functions which allows the manipulation of the channel list
             */
            $scope.allowSortable = function(){
                if($scope.isSortable){
                    $scope.isSortable = false;
                }else{
                    $scope.isSortable = true;
                }
            };
            /**
             * Redirect Detail Page
             */
            $scope.showDetails = function(channel){
              if(typeof channel.id != "undefined"){
                  $location.url("/detail/"+channel.id);
              }
            };

            /**
             * Updates EPG LIST
             */
            $scope.updateEpg = function(epglist){
                epglist.forEach(function(element, index, array){
                    if(element.order_nr != (index+1).toString()){
                        element.order_nr = (index+1).toString();
                    }
                });
                epg.updateEpg(epglist).then(function(data){
                   console.log(data);
                    $scope.isSortable = false;
                });
            };
        }]);

        senderCMSApp.controller('detailCtrl', ['$scope', '$routeParams', 'EPG','$location','$modal',
            function($scope, $routeParams,epg, $location, $modal){
                $scope.loggedIn = true;
                $scope.channel = {};
                $scope.transformRegion = "";
                $scope.types = globals.channelTypes;
                $scope.metaTypes = globals.metaTypes;
                $scope.recoGroups = globals.recoGroups;
                $scope.selectedType = {};
                $scope.streamTypes = globals.streamTypes;
                epg.getChannel($routeParams.id).then(function(channel){
                   $scope.channel = channel[0];
                   //To pre-select Sendertyp DropDown
                  var i;
                   for(i=0;i<$scope.types.length;i++){
                    if($scope.channel.channel_type === $scope.types[i].name){
                      $scope.channel.type = $scope.types[i];
                    }
                   }
                   //To pre-select Stream Typ of stream urls
                   for(i=0;i<$scope.streamTypes.length;i++){
                    for(var j =0;j<$scope.channel.stream_urls.length;j++){
                      if($scope.channel.stream_urls[j].type === $scope.streamTypes[i].type){
                         $scope.channel.stream_urls[j].type = $scope.streamTypes[i];
                      }
                    }
                   }
                   //to pre-select metadata type
                   for(i=0;i<$scope.metaTypes.length;i++){
                    if($scope.channel.sourcetype === $scope.metaTypes[i].name){
                      $scope.channel.sourcetype = $scope.metaTypes[i];
                    }
                   }
                   //Update Sender Gruppe
                   if($scope.channel.sourcetype.name == "rtv"){
                    for( i=0;i<$scope.recoGroups.length;i++){
                      if($scope.channel.reco_group === $scope.recoGroups[i].name){
                        $scope.channel.reco_group = $scope.recoGroups[i];
                      }
                    }
                   }

                }, function(error){
                    //ToDo
                });

                $scope.updateChannel = function(){
                    $scope.channel.sourcetype = $scope.channel.sourcetype.name; //override JSON Structure with basic String
                    if($scope.channel.sourcetype == 'rtv'){
                      $scope.channel.reco_group = $scope.channel.reco_group.name;
                    }else{
                      $scope.channel.reco_group = null;
                    }                   
                    $scope.channel.channel_type = $scope.channel.type.name;
                    delete $scope.channel.type;
                    if($scope.channel.channel_type == "IP-Stream"){
                      $scope.channel.stream_urls.forEach(function(element){
                        element.type = element.type.type;
                      });
                    }
                    epg.updateChannel($scope.channel).then(function(success){
                       $location.url("/start");
                    }, function(error){
                        //ToDo
                    });
                };

                $scope.removeStreamUrl = function(index){
                  //Removing element from array
                  console.log("Removing url at ", index);
                  $scope.channel.stream_urls.remove(index);
                };

                $scope.typeChange = function(){
                  console.log($scope.channel.type);
                };
                //Definition for ui.bootstrap modal directive!
                $scope.openModal = function (channel, $event) {

                  var modalInstance = $modal.open({
                    templateUrl: 'myModalContent.html',
                    controller: 'ModalInstanceCtrl'
                  });

                  modalInstance.result.then(function () {
                    epg.deleteChannel(channel).then(function(done){
                        $location.url("/start");
                    });  
                  }, function () {
                  });
                  // On recent browsers, only $event.stopPropagation() is needed
                  if ($event.stopPropagation) $event.stopPropagation();
                  if ($event.preventDefault) $event.preventDefault();
                  $event.cancelBubble = true;
                  $event.returnValue = false;
                };

            }

        ]);

        senderCMSApp.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 
          function($scope, $modalInstance){
              $scope.ok = function () {
                $modalInstance.close();
              };

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }]);

        senderCMSApp.controller('newCtrl', ['$scope', 'EPG','$location',
            function($scope, epg, $location){
                $scope.loggedIn = true;
                $scope.channel = new epg.Channel();
                $scope.transformRegion = "";
                $scope.types = globals.channelTypes;
                $scope.metaTypes = globals.metaTypes;
                $scope.recoGroups = globals.recoGroups;
                $scope.lastOrderNr = "";
                $scope.streamTypes = globals.streamTypes;
                //Get EPG list
                epg.getEPGList().then(function(data){
                    $scope.channel.order_nr = data.length + 1;
                }, function(error){
                    $scope.errorMessage = error.message;
                });
                //initialize some values
                $scope.channel.default = "0"; //default value: 0
                $scope.channel.tag = null;
                $scope.channel.tag2 = null;
                $scope.channel.stream_urls = [];
                $scope.create = function(){
                    $scope.channel.sourcetype = $scope.channel.sourcetype.name; //override JSON Structure with basic String
                    if($scope.channel.sourcetype == 'rtv'){
                      $scope.channel.reco_group = $scope.channel.reco_group.name;
                    }else{
                      $scope.channel.reco_group = null;
                      $scope.channel.tag = "NULL";
                    }
                    $scope.channel.channel_type = $scope.channel.type.name;
                    if($scope.channel.channel_type == "IP-Stream"){
                      $scope.channel.stream_urls.forEach(function(element){
                        element.type = element.type.type;
                      });
                    }
                    delete $scope.channel.type;
                    epg.createChannel($scope.channel).then(function(success){
                            $location.url("/start");
                        },
                    function(error){
                        //ToDo
                    });
                };
            }
        ]);

})();
(function () {
  "use strict";


  /* Directive */

  var soManagerAppDirectives = angular.module('ui.addstreamurl', []);
  /*
   * Directive for adding an IP-Stream entry for a new channel
   * template is a simple button and onclick new input options are displayed
   */
  soManagerAppDirectives.directive('ngAddStreamUrl', ['$compile',
    function ($compile) {
      return {
        restrict: 'EA',
        template: '<a class="btn btn-primary" type="submit"><span ng-if="isSortable" class="glyphicon glyphicon-align-justify"></span><span>IP-Stream hinzufügen </span></a>',
        link: function ($scope, element, attrs) {
          element.bind("click", function () {//Click Handler for Adding new ip stream information
            //new position of the ip stream entry
            if ($scope.channel.stream_urls === null) {
              $scope.channel.stream_urls = [];
            }
            //create empty entry
            $scope.channel.stream_urls.push({"type": "", "stream_url": ""});
            var lastElement = $scope.channel.stream_urls.length - 1;
            //Adding new DOM Elements after user is clicking to the button
            angular.element(this).parent().parent().after($compile('<br> <div class="form-group"><label for="soDesc" class="col-lg-2 control-label">Stream Typ</label><div class="col-lg-10"><select class="form-control" id="soDesc" ng-model="channel.stream_urls[' + lastElement + '].type" ng-options="t.name for t in streamTypes"><option value="">-- Bitte einen Sendertypen auswählen --</option></select></div></div><div class="form-group"><label for="soInput" class="col-sm-2 control-label">Stream Url</label><div class="col-sm-8"><input type="text" class="form-control" id="soInput" placeholder="Stream Url" ng-model="channel.stream_urls[' + lastElement + '].stream_url"></div></div><hr class="form-group-divider">')($scope));
          });
        }
      };
    }]);

  /*
   * Directive for adding an IP-Stream entry within Edit page
   * * creates an empty JSON representation
   */
  soManagerAppDirectives.directive('ngAddStreamUrlLight', ['$compile',
    function ($compile) {
      return {
        restrict: 'EA',
        template: '<a class="btn btn-primary" type="submit"><span ng-if="isSortable" class="glyphicon glyphicon-align-justify"></span><span>IP-Stream hinzufügen </span></a>',
        link: function ($scope, element, attrs) {
          element.bind("click", function () {
            //Click Handler for Adding new ip stream information
            //new position of the ip stream entry
            if ($scope.channel.stream_urls === null) {
              $scope.channel.stream_urls = [];
            }
            $scope.$apply(function () {
              //create empty entry
              $scope.channel.stream_urls.push({"fk_channel_id": $scope.channel.id, "type": "", "stream_url": ""});
            });
          });
        }
      };
    }]);
}());
/**
 * Created by ferittopcu on 13.11.14.
 * Directive for navigation bar
 */
(function () {

    var demoAppDirectives = angular.module('ui.navigationbar', []);

    demoAppDirectives.directive('ngNavigationBar', [
        function(){
            return {
                restrict: 'EA',
                scope:{
                  "ngState": "="
                },
                template: '<div class="navbar navbar-inverse"> <div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </button><a class="navbar-brand" href="#">Multithek Sender-CMS</a> </div><div class="navbar-collapse collapse navbar-responsive-collapse"><ul class="nav navbar-nav navbar-right" ng-if="ngState"><li><a href="#/">Senderübersicht</a></li><li><a href="#/new"><span class="glyphicon glyphicon-plus"></span> Neuen Kanal erstellen</a></li><li><a href="#/logout">Logout</a></li> </ul> </div> </div>',
            };
        }]);

})();
/**
 * Created by ferittopcu on 18.11.14.
 * The globals File stores all configuration parameters, e.g. HOST, PORT and API call Endpoints
 */

var globals =( function(){
    var HOST = "http://193.174.152.249";
    var PORT = "80";
    var epgMainUrl = "/cmsapi/epg/channellist";
    var channelDetailUrl = "/cmsapi/epg/channel";
    var groupNames = [
        {"name": "Hauptsender"},
        {"name": "Dritte"},
        {"name": "Regionale"},
        {"name": "Ausland"},
        {"name": "Sonstige Deutsche"}
    ];
    var metaTypes = [
        {"name": "rtv"},
        {"name": "static"}
    ];

    var channelTypes =[
        {"name": "IP-Stream"},
        {"name": "DVB-T"}
    ];

    var streamTypes = [
        {"name": "DASH (.mpd)", "type": "application/dash+xml"},
        {"name": "HLS (.m3u8)", "type": "application/vnd.apple.mpegurl"},
        {"name": "HDS (.f4m)", "type": "application/f4m"}
    ];

    function returnChannelUrl(id){
        if(typeof id == "undefined"){
            return HOST + channelDetailUrl;
        }else{
            return HOST + channelDetailUrl+"/"+id;
        }
    }

    return{
        host : HOST,
        port: PORT,
        recoGroups: groupNames,
        channelTypes: channelTypes,
        metaTypes: metaTypes,
        streamTypes: streamTypes,
        getEpgUrl: function(){ return HOST + epgMainUrl;},
        getChannelUrl: returnChannelUrl
    };
})();
/**
 * Created by ferittopcu on 13.11.14.
 */
(function(){

    var senderCMS = angular.module('senderCMS', ['senderCMSControllers', 'senderCMSServices', 'ui.sortable', 'ngResource', 'ngRoute', 'ui.navigationbar', 'ui.bootstrap', 'ui.addstreamurl']);

    senderCMS.config(['$routeProvider','$locationProvider',
        function($routeProvider, $locationProvider) {
//================================================
            // Define all the routes
            //================================================
            $routeProvider
                //Start page, where the user gets a list of registered SO and can add new ones
/*                .when('/', {
                    templateUrl: 'templates/login.html',
                    controller: 'loginCtrl'
                })*/
                .when('/', {
                    templateUrl: 'templates/start.html',
                    controller: 'startCtrl'
                })
                .when('/detail/:id',{
                    templateUrl: 'templates/detail.html',
                    controller: 'detailCtrl'
                })
                .when('/new', {
                    templateUrl: 'templates/new.html',
                    controller: 'newCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }]);

})();
/**
 * Created by ferittopcu on 13.11.14.
 * Main file for the definition of services
 * Project: senderCMS
 */
(function() {
    'use strict';

    var senderCMSServices = angular.module('senderCMSServices', ['ngResource']);

    senderCMSServices.factory('EPG', ['$resource','$http','$q','$rootScope',
        function($resource, $http, $q, $rootScope){

            var channel = function(){
                var channel = {
                    "title": "",
                    "type":"",
                    "stream_urls": [],
                    "region":""
                };
                return channel;
            };

            function parseJSON(data, headers){
                if(headers('Content-Type') == "text/html"){
                    var newdata = data.slice(3);
                    return JSON.parse(newdata);
                }
            }

            function getChannellist(){
                var deferred = $q.defer();
                $http({method: 'GET', url: globals.getEpgUrl()}).
                    success(function(data,status,headers, config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                       deferred.reject({error: status, message: "Konnte die EPG Liste nicht abrufen."});
                    });
                return deferred.promise;
            }

            function updateChannellist(list){
                var deferred = $q.defer();
                $http({method: "PUT", data: list, url: globals.getEpgUrl()}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function getChannelDetails(id){
                var deferred = $q.defer();
                $http({method: "GET", url: globals.getChannelUrl(id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function updateChannel(channel){
                var deferred = $q.defer();
                var channelCopy = {};
                angular.copy(channel, channelCopy);
                delete channelCopy.id; //Id is not necessary
                delete channelCopy.type; // type is only an angularjs internal helper -> not necessary
                $http({method: "PUT", data: channelCopy, url: globals.getChannelUrl(channel.id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function createChannel(channel){
                var deferred = $q.defer();
                $http({method: "POST", data: channel, url: globals.getChannelUrl()}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function deleteChannel(channel){
                var deferred = $q.defer();
                 $http({method: "DELETE", url: globals.getChannelUrl(channel.id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }


            return{
                getEPGList: getChannellist,
                getChannel: getChannelDetails,
                updateChannel: updateChannel,
                createChannel: createChannel,
                Channel: channel,
                updateEpg: updateChannellist,
                deleteChannel: deleteChannel
            };
        }
    ]);
})();
/**
 * Created by ferittopcu on 13.11.14.
 * Controller for senderCMS App
 */
(function () {

        var senderCMSApp = angular.module('senderCMSControllers', []);
        //Helper Class which adds a remove function for the basic array object
        Array.prototype.remove = function(from, to) {
          var rest = this.slice((to || from) + 1 || this.length);
          this.length = from < 0 ? this.length + from : from;
          return this.push.apply(this, rest);
        };
    /**
     * Our login app controller
     */
    senderCMSApp.controller('loginCtrl', ['$scope', '$rootScope','$location',
        function($scope, $rootScope, $location){
            $scope.username = "";
            $scope.password = "";
            $scope.loggedIn = false;
            $scope.loginError = false;
            $scope.errorMessage = "Sie haben falsche Benutzerdaten angegeben. Bitte kontaktieren sie den Administrator.!";
            $scope.login = function(){
                if($scope.username == "superuser" && $scope.password == "fame@fame"){
                    $location.url('/start');
                }else{
                    $scope.loginError = true;
                }
            };

            /**
             * Better function for dismissing alert box
             * standard bootstrap function is dismissing whole DOM element
             */
            $scope.dismissWarning = function(){
                $scope.loginError = false;
            };

        }]);

    /**
     *
     */
    senderCMSApp.controller('startCtrl', ['$scope', '$rootScope', '$location','EPG',
        function($scope, $rootScope, $location, epg){
            $scope.loggedIn = true;
            $scope.epglist = {};
            $scope.errorMessage = "";
            $scope.clickToChangeText = "Zum Bearbeiten der Senderliste hier klicken";
            $scope.smallText = "Zum Bearbeiten ";
            $scope.hasError = false;
            $scope.isSortable = false;
            $scope.sortableOptions = {
                update: function(e, ui) {
                    if (!$scope.isSortable) {
                        ui.item.sortable.cancel();
                    }
                }
            };

            //Get EPG list
            epg.getEPGList().then(function(data){
                $scope.epglist = data;
            }, function(error){
                $scope.errorMessage = error.message;
            });

            /**
             * Better function for dismissing alert box
             * standard bootstrap function is dismissing whole DOM element
             */
            $scope.dismissWarning = function(){
                $scope.loginError = false;
            };

            /**
             * Helper functions which allows the manipulation of the channel list
             */
            $scope.allowSortable = function(){
                if($scope.isSortable){
                    $scope.isSortable = false;
                }else{
                    $scope.isSortable = true;
                }
            };
            /**
             * Redirect Detail Page
             */
            $scope.showDetails = function(channel){
              if(typeof channel.id != "undefined"){
                  $location.url("/detail/"+channel.id);
              }
            };

            /**
             * Updates EPG LIST
             */
            $scope.updateEpg = function(epglist){
                epglist.forEach(function(element, index, array){
                    if(element.order_nr != (index+1).toString()){
                        element.order_nr = (index+1).toString();
                    }
                });
                epg.updateEpg(epglist).then(function(data){
                   console.log(data);
                    $scope.isSortable = false;
                });
            };
        }]);

        senderCMSApp.controller('detailCtrl', ['$scope', '$routeParams', 'EPG','$location','$modal',
            function($scope, $routeParams,epg, $location, $modal){
                $scope.loggedIn = true;
                $scope.channel = {};
                $scope.transformRegion = "";
                $scope.types = globals.channelTypes;
                $scope.metaTypes = globals.metaTypes;
                $scope.recoGroups = globals.recoGroups;
                $scope.selectedType = {};
                $scope.streamTypes = globals.streamTypes;
                epg.getChannel($routeParams.id).then(function(channel){
                   $scope.channel = channel[0];
                   //To pre-select Sendertyp DropDown
                  var i;
                   for(i=0;i<$scope.types.length;i++){
                    if($scope.channel.channel_type === $scope.types[i].name){
                      $scope.channel.type = $scope.types[i];
                    }
                   }
                   //To pre-select Stream Typ of stream urls
                   for(i=0;i<$scope.streamTypes.length;i++){
                    for(var j =0;j<$scope.channel.stream_urls.length;j++){
                      if($scope.channel.stream_urls[j].type === $scope.streamTypes[i].type){
                         $scope.channel.stream_urls[j].type = $scope.streamTypes[i];
                      }
                    }
                   }
                   //to pre-select metadata type
                   for(i=0;i<$scope.metaTypes.length;i++){
                    if($scope.channel.sourcetype === $scope.metaTypes[i].name){
                      $scope.channel.sourcetype = $scope.metaTypes[i];
                    }
                   }
                   //Update Sender Gruppe
                   if($scope.channel.sourcetype.name == "rtv"){
                    for( i=0;i<$scope.recoGroups.length;i++){
                      if($scope.channel.reco_group === $scope.recoGroups[i].name){
                        $scope.channel.reco_group = $scope.recoGroups[i];
                      }
                    }
                   }

                }, function(error){
                    //ToDo
                });

                $scope.updateChannel = function(){
                    $scope.channel.sourcetype = $scope.channel.sourcetype.name; //override JSON Structure with basic String
                    if($scope.channel.sourcetype == 'rtv'){
                      $scope.channel.reco_group = $scope.channel.reco_group.name;
                    }else{
                      $scope.channel.reco_group = null;
                    }                   
                    $scope.channel.channel_type = $scope.channel.type.name;
                    delete $scope.channel.type;
                    if($scope.channel.channel_type == "IP-Stream"){
                      $scope.channel.stream_urls.forEach(function(element){
                        element.type = element.type.type;
                      });
                    }
                    epg.updateChannel($scope.channel).then(function(success){
                       $location.url("/start");
                    }, function(error){
                        //ToDo
                    });
                };

                $scope.removeStreamUrl = function(index){
                  //Removing element from array
                  console.log("Removing url at ", index);
                  $scope.channel.stream_urls.remove(index);
                };

                $scope.typeChange = function(){
                  console.log($scope.channel.type);
                };
                //Definition for ui.bootstrap modal directive!
                $scope.openModal = function (channel, $event) {

                  var modalInstance = $modal.open({
                    templateUrl: 'myModalContent.html',
                    controller: 'ModalInstanceCtrl'
                  });

                  modalInstance.result.then(function () {
                    epg.deleteChannel(channel).then(function(done){
                        $location.url("/start");
                    });  
                  }, function () {
                  });
                  // On recent browsers, only $event.stopPropagation() is needed
                  if ($event.stopPropagation) $event.stopPropagation();
                  if ($event.preventDefault) $event.preventDefault();
                  $event.cancelBubble = true;
                  $event.returnValue = false;
                };

            }

        ]);

        senderCMSApp.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 
          function($scope, $modalInstance){
              $scope.ok = function () {
                $modalInstance.close();
              };

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };
          }]);

        senderCMSApp.controller('newCtrl', ['$scope', 'EPG','$location',
            function($scope, epg, $location){
                $scope.loggedIn = true;
                $scope.channel = new epg.Channel();
                $scope.transformRegion = "";
                $scope.types = globals.channelTypes;
                $scope.metaTypes = globals.metaTypes;
                $scope.recoGroups = globals.recoGroups;
                $scope.lastOrderNr = "";
                $scope.streamTypes = globals.streamTypes;
                //Get EPG list
                epg.getEPGList().then(function(data){
                    $scope.channel.order_nr = data.length + 1;
                }, function(error){
                    $scope.errorMessage = error.message;
                });
                //initialize some values
                $scope.channel.default = "0"; //default value: 0
                $scope.channel.tag = null;
                $scope.channel.tag2 = null;
                $scope.channel.stream_urls = [];
                $scope.create = function(){
                    $scope.channel.sourcetype = $scope.channel.sourcetype.name; //override JSON Structure with basic String
                    if($scope.channel.sourcetype == 'rtv'){
                      $scope.channel.reco_group = $scope.channel.reco_group.name;
                    }else{
                      $scope.channel.reco_group = null;
                      $scope.channel.tag = "NULL";
                    }
                    $scope.channel.channel_type = $scope.channel.type.name;
                    if($scope.channel.channel_type == "IP-Stream"){
                      $scope.channel.stream_urls.forEach(function(element){
                        element.type = element.type.type;
                      });
                    }
                    delete $scope.channel.type;
                    epg.createChannel($scope.channel).then(function(success){
                            $location.url("/start");
                        },
                    function(error){
                        //ToDo
                    });
                };
            }
        ]);

})();
(function () {
  "use strict";


  /* Directive */

  var soManagerAppDirectives = angular.module('ui.addstreamurl', []);
  /*
   * Directive for adding an IP-Stream entry for a new channel
   * template is a simple button and onclick new input options are displayed
   */
  soManagerAppDirectives.directive('ngAddStreamUrl', ['$compile',
    function ($compile) {
      return {
        restrict: 'EA',
        template: '<a class="btn btn-primary" type="submit"><span ng-if="isSortable" class="glyphicon glyphicon-align-justify"></span><span>IP-Stream hinzufügen </span></a>',
        link: function ($scope, element, attrs) {
          element.bind("click", function () {//Click Handler for Adding new ip stream information
            //new position of the ip stream entry
            if ($scope.channel.stream_urls === null) {
              $scope.channel.stream_urls = [];
            }
            //create empty entry
            $scope.channel.stream_urls.push({"type": "", "stream_url": ""});
            var lastElement = $scope.channel.stream_urls.length - 1;
            //Adding new DOM Elements after user is clicking to the button
            angular.element(this).parent().parent().after($compile('<br> <div class="form-group"><label for="soDesc" class="col-lg-2 control-label">Stream Typ</label><div class="col-lg-10"><select class="form-control" id="soDesc" ng-model="channel.stream_urls[' + lastElement + '].type" ng-options="t.name for t in streamTypes"><option value="">-- Bitte einen Sendertypen auswählen --</option></select></div></div><div class="form-group"><label for="soInput" class="col-sm-2 control-label">Stream Url</label><div class="col-sm-8"><input type="text" class="form-control" id="soInput" placeholder="Stream Url" ng-model="channel.stream_urls[' + lastElement + '].stream_url"></div></div><hr class="form-group-divider">')($scope));
          });
        }
      };
    }]);

  /*
   * Directive for adding an IP-Stream entry within Edit page
   * * creates an empty JSON representation
   */
  soManagerAppDirectives.directive('ngAddStreamUrlLight', ['$compile',
    function ($compile) {
      return {
        restrict: 'EA',
        template: '<a class="btn btn-primary" type="submit"><span ng-if="isSortable" class="glyphicon glyphicon-align-justify"></span><span>IP-Stream hinzufügen </span></a>',
        link: function ($scope, element, attrs) {
          element.bind("click", function () {
            //Click Handler for Adding new ip stream information
            //new position of the ip stream entry
            if ($scope.channel.stream_urls === null) {
              $scope.channel.stream_urls = [];
            }
            $scope.$apply(function () {
              //create empty entry
              $scope.channel.stream_urls.push({"fk_channel_id": $scope.channel.id, "type": "", "stream_url": ""});
            });
          });
        }
      };
    }]);
}());
/**
 * Created by ferittopcu on 13.11.14.
 * Directive for navigation bar
 */
(function () {

    var demoAppDirectives = angular.module('ui.navigationbar', []);

    demoAppDirectives.directive('ngNavigationBar', [
        function(){
            return {
                restrict: 'EA',
                scope:{
                  "ngState": "="
                },
                template: '<div class="navbar navbar-inverse"> <div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </button><a class="navbar-brand" href="#">Multithek Sender-CMS</a> </div><div class="navbar-collapse collapse navbar-responsive-collapse"><ul class="nav navbar-nav navbar-right" ng-if="ngState"><li><a href="#/">Senderübersicht</a></li><li><a href="#/new"><span class="glyphicon glyphicon-plus"></span> Neuen Kanal erstellen</a></li><li><a href="#/logout">Logout</a></li> </ul> </div> </div>',
            };
        }]);

})();
/**
 * Created by ferittopcu on 18.11.14.
 * The globals File stores all configuration parameters, e.g. HOST, PORT and API call Endpoints
 */

var globals =( function(){
    var HOST = "http://193.174.152.249";
    var PORT = "80";
    var epgMainUrl = "/cmsapi/epg/channellist";
    var channelDetailUrl = "/cmsapi/epg/channel";
    var groupNames = [
        {"name": "Hauptsender"},
        {"name": "Dritte"},
        {"name": "Regionale"},
        {"name": "Ausland"},
        {"name": "Sonstige Deutsche"}
    ];
    var metaTypes = [
        {"name": "rtv"},
        {"name": "static"}
    ];

    var channelTypes =[
        {"name": "IP-Stream"},
        {"name": "DVB-T"}
    ];

    var streamTypes = [
        {"name": "DASH (.mpd)", "type": "application/dash+xml"},
        {"name": "HLS (.m3u8)", "type": "application/vnd.apple.mpegurl"},
        {"name": "HDS (.f4m)", "type": "application/f4m"}
    ];

    function returnChannelUrl(id){
        if(typeof id == "undefined"){
            return HOST + channelDetailUrl;
        }else{
            return HOST + channelDetailUrl+"/"+id;
        }
    }

    return{
        host : HOST,
        port: PORT,
        recoGroups: groupNames,
        channelTypes: channelTypes,
        metaTypes: metaTypes,
        streamTypes: streamTypes,
        getEpgUrl: function(){ return HOST + epgMainUrl;},
        getChannelUrl: returnChannelUrl
    };
})();
/**
 * Created by ferittopcu on 13.11.14.
 */
(function(){

    var senderCMS = angular.module('senderCMS', ['senderCMSControllers', 'senderCMSServices', 'ui.sortable', 'ngResource', 'ngRoute', 'ui.navigationbar', 'ui.bootstrap', 'ui.addstreamurl']);

    senderCMS.config(['$routeProvider','$locationProvider',
        function($routeProvider, $locationProvider) {
//================================================
            // Define all the routes
            //================================================
            $routeProvider
                //Start page, where the user gets a list of registered SO and can add new ones
/*                .when('/', {
                    templateUrl: 'templates/login.html',
                    controller: 'loginCtrl'
                })*/
                .when('/', {
                    templateUrl: 'templates/start.html',
                    controller: 'startCtrl'
                })
                .when('/detail/:id',{
                    templateUrl: 'templates/detail.html',
                    controller: 'detailCtrl'
                })
                .when('/new', {
                    templateUrl: 'templates/new.html',
                    controller: 'newCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                });
        }]);

})();
/**
 * Created by ferittopcu on 13.11.14.
 * Main file for the definition of services
 * Project: senderCMS
 */
(function() {
    'use strict';

    var senderCMSServices = angular.module('senderCMSServices', ['ngResource']);

    senderCMSServices.factory('EPG', ['$resource','$http','$q','$rootScope',
        function($resource, $http, $q, $rootScope){

            var channel = function(){
                var channel = {
                    "title": "",
                    "type":"",
                    "stream_urls": [],
                    "region":""
                };
                return channel;
            };

            function parseJSON(data, headers){
                if(headers('Content-Type') == "text/html"){
                    var newdata = data.slice(3);
                    return JSON.parse(newdata);
                }
            }

            function getChannellist(){
                var deferred = $q.defer();
                $http({method: 'GET', url: globals.getEpgUrl()}).
                    success(function(data,status,headers, config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                       deferred.reject({error: status, message: "Konnte die EPG Liste nicht abrufen."});
                    });
                return deferred.promise;
            }

            function updateChannellist(list){
                var deferred = $q.defer();
                $http({method: "PUT", data: list, url: globals.getEpgUrl()}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function getChannelDetails(id){
                var deferred = $q.defer();
                $http({method: "GET", url: globals.getChannelUrl(id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function updateChannel(channel){
                var deferred = $q.defer();
                var channelCopy = {};
                angular.copy(channel, channelCopy);
                delete channelCopy.id; //Id is not necessary
                delete channelCopy.type; // type is only an angularjs internal helper -> not necessary
                $http({method: "PUT", data: channelCopy, url: globals.getChannelUrl(channel.id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function createChannel(channel){
                var deferred = $q.defer();
                $http({method: "POST", data: channel, url: globals.getChannelUrl()}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            function deleteChannel(channel){
                var deferred = $q.defer();
                 $http({method: "DELETE", url: globals.getChannelUrl(channel.id)}).
                    success(function(data, status, headers,config){
                        deferred.resolve(data);
                    }).
                    error(function(data, status, headers, config){
                        deferred.reject(data);
                    });
                return deferred.promise;
            }


            return{
                getEPGList: getChannellist,
                getChannel: getChannelDetails,
                updateChannel: updateChannel,
                createChannel: createChannel,
                Channel: channel,
                updateEpg: updateChannellist,
                deleteChannel: deleteChannel
            };
        }
    ]);
})();