/**
 * Created by ferittopcu on 13.11.14.
 */
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        paths: {
          src: 'src/bower_components/*/'
        },
        jshint: {
            // define the files to lint
            files: ['gruntfile.js','src/js/**/*.js', 'src/js/*.js' ],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
            }
        },
        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';'
            },
            dist: {
                // the files to concatenate
                src: ['<%= paths.src %>/*.min.js','src/js/annotated/*.js'],
                // the location of the resulting JS file
                dest: 'dist/<%= pkg.name %>.js'
            }
        },
        ngAnnotate:{
            options : {

            },
          dist: {
            files: [
              {
                src: ['src/js/**/*.js', 'src/js/*.js'],
                dest: 'src/js/annotated/app.annotated.js'
              }
            ]

          }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                //Use concatenated file
                src: 'dist/<%= pkg.name %>.js',
                dest: 'build/src/<%= pkg.name %>.min.js'
            }
        },
        'wiredep': {
            'task':{
                'src':[
                    'src/**/*.html'
                ],
                'options':{

                }
            }
        },
        connect:{
            server:{
                options:{
                    port: 8000,
                    base: {
                        path: 'build/src',
                        options:{
                            index: 'index.html',
                            maxAge: 300000
                        }
                    },
                    keepalive: true
                }
            },
            dev:{
              options:{
                port: 8000,
                base: {
                  path: 'src',
                  options:{
                    index: 'index.html',
                    maxAge: 300000
                  }
                },
                keepalive: true
              }
            }
        },
        copy:{
            bower:{
                files:[
                    {expand: true, src: ['bower_components/**'], dest: 'src'}
                ]
            },
            build: {
              files:[
                {expand: true, src: ['src/index.html'], dest: 'build'},
                {expand: true, flatten:true, src: ['src/css/*.css', 'src/bower_components/bootstrap/dist/css/*.{css, css.map}'], dest: 'build/src/css/'}
              ]
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Default task(s).
    grunt.registerTask('build', [
        'jshint',
        'concat',
        'uglify',
        'copy:build'
    ]);

  grunt.registerTask('serve', [
      'build',
      'connect'
  ]);

  grunt.registerTask('serve:dev', [
      'jshint',
      'copy:bower',
      'connect:dev'
  ]);

};